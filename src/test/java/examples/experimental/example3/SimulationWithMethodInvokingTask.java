package examples.experimental.example3;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import examples.experimental.example3.classes.MyCallerWithMethodInvokingTask;

import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.breakstrategy.impl.LimitedCurrentAverageExecutionTimeStrategy;


public class SimulationWithMethodInvokingTask {
  public static void main(String[] args) throws ExecutionException, InterruptedException {
    HashSet<Integer> failedPings = new HashSet<>();

    BreakHandler<Void> failedPingsStoringHandler = (circuitBreaker, task, breakStrategy, executionContext) -> {
      failedPings.add(task.hashCode());
      return null;
    };

    BreakStrategy<Void> breakStrategy = new LimitedCurrentAverageExecutionTimeStrategy<Void>(700);


    MyCallerWithMethodInvokingTask myCaller = new MyCallerWithMethodInvokingTask(failedPingsStoringHandler,
      breakStrategy);

    // simulate access by 10 threads

    ExecutorService executorService = Executors.newWorkStealingPool(10);

    List<Future> responses = new ArrayList<>();

    long start = System.currentTimeMillis();

    // simulate multiple execution of the method by different threads at the same time
    for (int i = 0; i < 100; i++) {
      AtomicInteger pingId = new AtomicInteger(i);

      Future<?> response = executorService.submit(() -> {
        try {
          myCaller.ping(pingId.get());
        } catch (Exception e) {
          System.out.println("Exception while pinging #" + pingId.get());
        }

      });
      responses.add(response);
    }


    executorService.shutdown();
    executorService.awaitTermination(2, TimeUnit.MINUTES);

    long stop = System.currentTimeMillis();
    System.out.println("Failed pings: " + failedPings.size());
    System.out.printf("Finished in %d seconds", (int) (stop - start) / 1000);

  }

}
