package examples.experimental.example3.classes;

import java.io.IOException;

import java.util.Random;


public class PingService {
  private Random random = new Random();


  public void sendPing(Integer pingId) throws IOException {
    try {
      Thread.sleep(500 + random.nextInt(1000));
    } catch (InterruptedException e) {
    }

    System.out.println("ping made #" + pingId);
  }

}
