package examples.experimental.example3.classes;

import java.io.IOException;

import net.secodo.jcircuitbreaker.breaker.CircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.impl.DefaultCircuitBreaker;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;
import net.secodo.jcircuitbreaker.task.experimental.MethodInvokingTask;


public class MyCallerWithMethodInvokingTask {
  private final PingService pingService;
  private final CircuitBreaker<Void> circuitBreaker;
  private final BreakHandler<Void> breakHandler;
  private final BreakStrategy<Void> breakStrategy;

  public MyCallerWithMethodInvokingTask(BreakHandler<Void> breakHandler, BreakStrategy<Void> breakStrategy) {
    this.pingService = new PingService();

    // prepare the circuit breaker
    this.circuitBreaker = new DefaultCircuitBreaker<>();
    this.breakStrategy = breakStrategy;
    this.breakHandler = breakHandler;

  }

  public void ping(int pingId) throws IOException {
    final Task<Void> task;
    try {
      task = new MethodInvokingTask(pingService, "sendPing", Void.class, pingId);
    } catch (NoSuchMethodException e) {
      throw new RuntimeException("Wrong method defintion passed to method invoking task", e);
    }

    try {
      circuitBreaker.execute(task, breakStrategy, breakHandler);
    } catch (TaskExecutionException e) {
      System.out.println("Calling ping resulted in exception: " + e.getTaskException());
      throw new IOException(e.getTaskException().getMessage(), e.getTaskException());
    }

  }

}
