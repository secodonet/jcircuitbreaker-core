package examples.example2;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import examples.example2.classes.MyCaller;


public class Simulation {
  public static void main(String[] args) throws ExecutionException, InterruptedException {
    MyCaller myCaller = new MyCaller();

    // simulate access by 10 threads

    ExecutorService executorService = Executors.newWorkStealingPool(10);
    List<Future> responses = new ArrayList<>();

    // simulate multiple execution of the method by different threads
    long start = System.currentTimeMillis();

    for (int i = 0; i < 100; i++) {
      AtomicInteger pingId = new AtomicInteger(i);

      executorService.submit(() -> {
        try {
          myCaller.ping(pingId.get());
        } catch (IOException e) {
          System.out.println("Exception while handling ping #" + pingId.get());
        }
      });
    }


    executorService.shutdown();
    executorService.awaitTermination(2, TimeUnit.MINUTES);

    long stop = System.currentTimeMillis();
    System.out.printf("Finished in %d seconds", (int) (stop - start) / 1000);

  }

}
