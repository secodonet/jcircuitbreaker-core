package examples.example2.classes;

import java.io.IOException;
import java.util.HashSet;

import net.secodo.jcircuitbreaker.breaker.CircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.impl.DefaultCircuitBreaker;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.breakstrategy.impl.LimitedCurrentAverageExecutionTimeStrategy;
import net.secodo.jcircuitbreaker.breakstrategy.impl.SingleExecutionAllowedStrategy;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.VoidTask;


public class MyCallerWithCircuitBreaker {
  private final PingService pingService;
  private final CircuitBreaker<Void> circuitBreaker;
  private final BreakHandler<Void> breakHandler;
  private final BreakStrategy<Void> breakStrategy;

  public MyCallerWithCircuitBreaker(final HashSet<Integer> failedPings) {
    BreakHandler<Void> breakHandler = (circuitBreaker, task, breakStrategy, executionContext) -> {
      failedPings.add(task.hashCode());
      return null;
    };

    BreakStrategy<Void> breakStrategy = new LimitedCurrentAverageExecutionTimeStrategy<Void>(700);
    //BreakStrategy<Void> breakStrategy = new SingleExecutionAllowedStrategy<>();

    this.pingService = new PingService();

    // prepare the circuit breaker
    this.circuitBreaker = new DefaultCircuitBreaker<>();
    this.breakStrategy = breakStrategy;
    this.breakHandler = breakHandler;
  }

  public void ping(int pingId) throws IOException {
    try {
      circuitBreaker.execute((VoidTask) () -> pingService.sendPing(pingId), breakStrategy, breakHandler);
    } catch (TaskExecutionException e) {
      System.out.println("Calling ping resulted in exception: " + e.getTaskException());
      throw new IOException(e.getTaskException().getMessage(), e.getTaskException());
    }

  }

}
