package examples.example2.classes;

import java.io.IOException;


public class MyCaller {
  private final PingService pingService;

  public MyCaller() {
    this.pingService = new PingService();
  }

  public void ping(int pingId) throws IOException {
    pingService.sendPing(pingId);
  }

}
