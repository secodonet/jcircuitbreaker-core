package examples.example2;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import examples.example2.classes.MyCallerWithCircuitBreaker;


public class SimulationWithCircuitBreaker {
  public static void main(String[] args) throws ExecutionException, InterruptedException {
    HashSet<Integer> failedPings = new HashSet<>();
    MyCallerWithCircuitBreaker myCaller = new MyCallerWithCircuitBreaker(failedPings);

    // simulate access by 10 threads

    ExecutorService executorService = Executors.newWorkStealingPool(10);

    long start = System.currentTimeMillis();

    // simulate multiple execution of the method by different threads at the same time
    for (int i = 0; i < 100; i++) {
      AtomicInteger pingId = new AtomicInteger(i);

      executorService.submit(() -> {
        try {
          myCaller.ping(pingId.get());
        } catch (IOException e) {
          System.out.println("Exception while pinging #" + pingId.get());
        }

      });
    }


    executorService.shutdown();
    executorService.awaitTermination(2, TimeUnit.MINUTES);

    long stop = System.currentTimeMillis();
    System.out.println("Failed pings: " + failedPings.size());
    System.out.printf("Finished in %d seconds", (int) (stop - start) / 1000);

  }

}
