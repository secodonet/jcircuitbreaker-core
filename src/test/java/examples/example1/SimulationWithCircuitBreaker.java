package examples.example1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import examples.example1.classes.MyCallerWithCircuitBreaker;


public class SimulationWithCircuitBreaker {
  public static void main(String[] args) throws ExecutionException, InterruptedException {
    MyCallerWithCircuitBreaker myCaller = new MyCallerWithCircuitBreaker();

    // simulate access by 10 threads

    ExecutorService executorService = Executors.newWorkStealingPool(10);
    List<Future<Long>> responses = new ArrayList<>();

    // simulate multiple execution of the method by different threads
    long start = System.currentTimeMillis();

    for (int i = 0; i < 100; i++) {
      AtomicInteger callId = new AtomicInteger(i);

      Future<Long> response = executorService.submit(() -> myCaller.runService(callId.get()));
      responses.add(response);
    }

    ArrayList<Long> responseValues = new ArrayList<>(responses.size());
    for (int i = 0; i < responses.size(); i++) {
      Long response = responses.get(i).get();
      responseValues.add(response);
    }

    for (int i = 0; i < responseValues.size(); i++) {
      System.out.println("Response for call #:" + i + " is: " + responseValues.get(i));
    }


    executorService.shutdown();
    executorService.awaitTermination(2, TimeUnit.MINUTES);

    long stop = System.currentTimeMillis();
    System.out.printf("Finished in %d seconds", (int) (stop - start) / 1000);
  }

}
