package examples.example1.classes;

import net.secodo.jcircuitbreaker.breaker.CircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.impl.DefaultCircuitBreaker;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakhandler.impl.ReturnStaticValueHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.breakstrategy.impl.LimitedConcurrentExecutionsStrategy;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;

public class MyCallerWithCircuitBreaker {

  private final SomeServiceClass myService;
  private final CircuitBreaker<Long> circuitBreaker;
  private final BreakHandler<Long> breakHandler;
  private final BreakStrategy<Long> breakStrategy;

  public MyCallerWithCircuitBreaker() {
    this.myService = new SomeServiceClass();

    // prepare the circuit breaker
    this.circuitBreaker = new DefaultCircuitBreaker<>();
    this.breakHandler = new ReturnStaticValueHandler<>(-1l);
    this.breakStrategy = new LimitedConcurrentExecutionsStrategy<>(3l); // allow only 3 executions in parallel


  }

  public Long runService(int serviceParam)  {

    try {
      return circuitBreaker.execute(() -> myService.somePossiblyLongRunningMethod(serviceParam), breakStrategy,
        breakHandler);
    } catch (TaskExecutionException e) {
      System.out.println("Calling somePossiblyLongRunningMethod resulted in exception: " + e.getTaskException());
      throw new RuntimeException(e.getTaskException().getMessage(), e.getTaskException());
    }
  }

}
