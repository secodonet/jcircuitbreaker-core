package examples.example1.classes;

public class MyCaller {

  private final SomeServiceClass myService;

  public MyCaller() {
    this.myService = new SomeServiceClass();
  }

  public Long runService(int serviceParam)  {
     return myService.somePossiblyLongRunningMethod(serviceParam);
  }

}
