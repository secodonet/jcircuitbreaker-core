package examples.example1.classes;

public class SomeServiceClass {
  public Long somePossiblyLongRunningMethod(int callId) {
    // some heavy and long running operation here

    System.out.println("Execution started of call #" + callId);

    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {

    }

    System.out.println("Execution finished of call #" + callId);

    return (long) 1;
  }
}