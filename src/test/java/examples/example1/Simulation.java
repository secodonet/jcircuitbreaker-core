package examples.example1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import examples.example1.classes.MyCaller;


public class Simulation {
  public static void main(String[] args) throws ExecutionException, InterruptedException {
    MyCaller myCaller = new MyCaller();

    // simulate access by 10 threads

    ExecutorService executorService = Executors.newWorkStealingPool(10);
    List<Future<Long>> responses = new ArrayList<>();

    // simulate multiple execution of the method by different threads
    long start = System.currentTimeMillis();

    for (int i = 0; i < 100; i++) {
      AtomicInteger callId = new AtomicInteger(i);

      Future<Long> response = executorService.submit(() -> myCaller.runService(callId.get()));
      responses.add(response);
    }

    for (int i = 0; i < responses.size(); i++) {
      Long response = responses.get(i).get();
      System.out.println("Response for call #:" + i + " is: " + response);
    }


    executorService.shutdown();
    executorService.awaitTermination(2, TimeUnit.MINUTES);

    long stop = System.currentTimeMillis();
    System.out.printf("Finished in %d seconds", (int) (stop - start) / 1000);

  }

}
