package examples.workshop;


public class HowToAccess {
  public static void main(String[] args) throws Exception {
    String myText = "How to access this?";
    int i = 6;

    final SomePrintingClass printer = new SomePrintingClass();

    Runnable r = () -> { printer.print(myText, i); };

    Runnable myr = MyRunnable.wrap(r);

    inspect(myr);
  }


  private static void inspect(Runnable runnable) throws IllegalAccessException {
    // I have reference only to printer... can I access some
    runnable.run();
  }


}
