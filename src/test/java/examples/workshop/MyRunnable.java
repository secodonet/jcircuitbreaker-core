package examples.workshop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


public class MyRunnable implements Runnable, InvocationHandler {
  private final Runnable runnable;

  public MyRunnable(Runnable runnable) {
    this.runnable = runnable;
  }

  public static Runnable wrap(Runnable runnable) {
    return (Runnable) Proxy.newProxyInstance(runnable.getClass().getClassLoader(),
      new Class[] { Runnable.class },
      new MyRunnable(runnable));
  }


  @Override
  public void run() {
    System.out.println("Calling real run");
    runnable.run();
  }

  public void inspect() {
    System.out.println("Calling real run");
    runnable.run();
  }

  @Override
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    System.out.println("call cal cal");
    return method.invoke(this, args);
  }
}
