package net.secodo.jcircuitbreaker.breakstrategy.impl;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutedTask;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SuppressWarnings("unchecked")
public class LimitedConcurrentExecutionsStrategyTest {
  @Test
  public void shouldBreakWhenTwoExecutionsAllowedAndThirdTaskIsAboutToExecute() {
    // given
    final int maxSupportedExecutions = 2;
    final BreakStrategy breakStrategy = new LimitedConcurrentExecutionsStrategy(maxSupportedExecutions);
    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);

    final List<ExecutedTask> executionsInProgress = new ArrayList<ExecutedTask>() {{
      add(mock(ExecutedTask.class));
      add(mock(ExecutedTask.class));
    }};

    assertTrue(executionsInProgress.size() == maxSupportedExecutions);

    when(executionContext.getExecutionsInProgress()).thenReturn(executionsInProgress);

    // when
    final boolean breakResult = breakStrategy.shouldBreak(task, executionContext);

    // then
    assertThat(breakResult, equalTo(true));
  }

  @Test
  public void shouldBreakWhenMoreThenAllowedNumberOfExecutionsAreInProgress() {
    // given
    final int maxSupportedExecutions = 20;
    final BreakStrategy breakStrategy = new LimitedConcurrentExecutionsStrategy(maxSupportedExecutions);
    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);

    final List<ExecutedTask> executionsInProgress = new ArrayList<>();
    IntStream.range(0, maxSupportedExecutions).forEach(i ->
        executionsInProgress.add(new ExecutedTask(mock(Task.class), i)));

    assertTrue(executionsInProgress.size() == maxSupportedExecutions);

    when(executionContext.getExecutionsInProgress()).thenReturn(executionsInProgress);

    // when
    final boolean breakResult = breakStrategy.shouldBreak(task, executionContext);

    // then
    assertThat(breakResult, equalTo(true));
  }

  @Test
  public void shouldNotBreakWhenNumberOfAllowedExecutionsIsNotExceeded() {
    // given
    final int maxSupportedExecutions = 20;
    final BreakStrategy breakStrategy = new LimitedConcurrentExecutionsStrategy(maxSupportedExecutions);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    final Task task = mock(Task.class);

    final List<ExecutedTask> executionsInProgress = new ArrayList<>();
    IntStream.range(0, maxSupportedExecutions - 1).forEach(i ->
        executionsInProgress.add(new ExecutedTask(mock(Task.class), i)));
    
    assertTrue(executionsInProgress.size() < maxSupportedExecutions);

    when(executionContext.getExecutionsInProgress()).thenReturn(executionsInProgress);

    // when
    final boolean breakResult = breakStrategy.shouldBreak(task, executionContext);

    // then
    assertThat(breakResult, equalTo(false));
  }


}
