package net.secodo.jcircuitbreaker.breakstrategy.impl.dsl;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SuppressWarnings("unchecked")
public class DslAndStrategyTest {
  @Test
  public void shouldBreakIfBothStrategiesDecidesToBreak() throws Exception {
    // given
    BreakStrategy strategy1 = mock(BreakStrategy.class);
    BreakStrategy strategy2 = mock(BreakStrategy.class);

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);

    // when
    final boolean breakResult = new DslAndStrategy(strategy1, strategy2).shouldBreak(mock(Task.class),
      mock(ExecutionContext.class));

    // then
    Assert.assertThat(breakResult, equalTo(true));
  }

  @Test
  public void shouldNotBreakIfFirstStrategyDecidesToBreakAndSecondNot() throws Exception {
    // given
    BreakStrategy strategy1 = mock(BreakStrategy.class);
    BreakStrategy strategy2 = mock(BreakStrategy.class);

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);

    // when
    final boolean breakResult = new DslAndStrategy(strategy1, strategy2).shouldBreak(mock(Task.class),
      mock(ExecutionContext.class));

    // then
    Assert.assertThat(breakResult, equalTo(false));
  }

  @Test
  public void shouldNotBreakIfFirstStrategyDecidesNotToBreakAndSecondToBreak() throws Exception {
    // given
    BreakStrategy strategy1 = mock(BreakStrategy.class);
    BreakStrategy strategy2 = mock(BreakStrategy.class);

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);

    // when
    final boolean breakResult = new DslAndStrategy(strategy1, strategy2).shouldBreak(mock(Task.class),
      mock(ExecutionContext.class));

    // then
    Assert.assertThat(breakResult, equalTo(false));
  }

  @Test
  public void shouldNotBreakIfBothStrategiesDecidesNotToBreak() throws Exception {
    // given
    BreakStrategy strategy1 = mock(BreakStrategy.class);
    BreakStrategy strategy2 = mock(BreakStrategy.class);

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);

    // when
    final boolean breakResult = new DslAndStrategy(strategy1, strategy2).shouldBreak(mock(Task.class),
      mock(ExecutionContext.class));

    // then
    Assert.assertThat(breakResult, equalTo(false));
  }


}
