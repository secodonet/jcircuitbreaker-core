package net.secodo.jcircuitbreaker.breakstrategy.impl;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutedTask;
import net.secodo.jcircuitbreaker.task.Task;
import net.secodo.jcircuitbreaker.util.TimeUtil;
import org.junit.Test;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.IntStream;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;


@SuppressWarnings("unchecked")
public class LimitedCurrentAverageExecutionTimeStrategyTest {
  @Test
  public void shouldNotBreakWhenThereIsNoExecutionYet() throws Exception {
    //given
    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(Collections.EMPTY_LIST);

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(10000);

    // when
    final boolean shouldBreak = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreak, equalTo(false));


  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_1() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 0; // all times will be taken into consideration

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(false));


  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with10PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 10; // !!! now 10 percent of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(false));


  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with20PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 20; // !!! now 20 percent of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(false));


  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with30PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 30; // !!! now 30 percents of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(false));
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with40PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 40; // !!! now 40 percents of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(false));
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with50PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 50; // !!! now 50 percents of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext); // THIS IS BOUNDARY VALUE - AVERAGE TIME == 8 (as maxAllowedExecutionTimeMilis)

    // then
    assertThat(shouldBreakResult, equalTo(false));
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with60PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 60; // !!! now 60 percents of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(true)); // now should break because 8.5 > maxAllowedExecutionTimeMilis (which is 8)
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with70PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 70; // !!! now 70 percents of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(true));
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with80PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 80; // !!! now 80 percents of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(true));
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with90PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 90; // !!! now 90 percents of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(true));
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with99PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 99; // !!! now 99 percents of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(false)); // false becasue there are no times to take into consideration
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with100PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 100; // !!! now 100 percents of times should be skipped

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(false)); // false becasue there are no times to take into consideration
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_with105PercentOfTimesToSkip() throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = 105; // !!! now 105 percents of times should be skipped - this is transformed to 0%

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(false)); // false because there all times were taken into consideration - because 105% transforms to 0%
  }

  @Test
  public void shouldNotBreakWhenAverageExecutionTimeIsLessThenMaxTime_withMinus1PercentOfTimesToSkip()
                                                                                              throws Exception {
    // given
    final long maxAllowedExecutionTimeMilis = 8;
    final int percentageOfLongestTimesToSkip = -1; // !!! now -1 percentsof times should be skipped - this is transformed to 0%

    final Collection<ExecutedTask> tasksInProgress = new ArrayList<>();

    final Task task = mock(Task.class);
    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getExecutionsInProgress()).thenReturn(tasksInProgress);

    final TimeUtil timeUtil = mock(TimeUtil.class);


    IntStream.range(0, 10).forEach(i -> {
      ExecutedTask executedTask = new ExecutedTask(mock(Task.class), i);
      tasksInProgress.add(executedTask);
    });

    final LimitedCurrentAverageExecutionTimeStrategy strategy = new LimitedCurrentAverageExecutionTimeStrategy(
      maxAllowedExecutionTimeMilis,
      percentageOfLongestTimesToSkip);

    final Field timeUtilField = LimitedCurrentAverageExecutionTimeStrategy.class.getDeclaredField("timeUtil");
    timeUtilField.setAccessible(true);
    timeUtilField.set(strategy, timeUtil);

    final long currentTimeForStrategy = 10;
    when(timeUtil.getCurrentTimeMilis()).thenReturn(currentTimeForStrategy);

    // when
    final boolean shouldBreakResult = strategy.shouldBreak(task, executionContext);

    // then
    assertThat(shouldBreakResult, equalTo(false)); // false because there all times were taken into consideration - because 105% transforms to 0%
  }
}
