package net.secodo.jcircuitbreaker.breakstrategy.impl;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutedTask;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SingleExecutionAllowedStrategyTest {

  @Test
  public void shouldNotAllowExecutionIfThereIsAnotherInProgress() {
    // given
    SingleExecutionAllowedStrategy<Integer> breakStrategy = new SingleExecutionAllowedStrategy<>();
    ExecutionContext<Integer> executionContext = mock(ExecutionContext.class);

    ArrayList<ExecutedTask<Integer>> executedTasks = new ArrayList<ExecutedTask<Integer>>() {{ add(mock(ExecutedTask.class)); }};
    when(executionContext.getExecutionsInProgress()).thenReturn(executedTasks);

    // when
    boolean shouldBreak = breakStrategy.shouldBreak(mock(Task.class), executionContext);

    // then
    assertThat(shouldBreak, is(true));

  }


}