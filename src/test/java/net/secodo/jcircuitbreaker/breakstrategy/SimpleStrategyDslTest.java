package net.secodo.jcircuitbreaker.breakstrategy;

import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static net.secodo.jcircuitbreaker.breakstrategy.SimpleStrategyDsl.allOf;
import static net.secodo.jcircuitbreaker.breakstrategy.SimpleStrategyDsl.anyOf;


@SuppressWarnings("unchecked")
public class SimpleStrategyDslTest {
  @Test
  public void shouldBreakCorrectlyWhenUsingDsl() {
    // given
    BreakStrategy strategy1 = mock(BreakStrategy.class);
    BreakStrategy strategy2 = mock(BreakStrategy.class);
    BreakStrategy strategy3 = mock(BreakStrategy.class);
    BreakStrategy strategy4 = mock(BreakStrategy.class);


    final BreakStrategy resultingStrategy = anyOf(strategy1, strategy2, allOf(strategy3, strategy4));

    // test 1

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy3.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy4.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);

    boolean resultingValue = resultingStrategy.shouldBreak(mock(Task.class), mock(ExecutionContext.class));

    assertThat(resultingValue, equalTo(true));

    // test 2

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy3.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy4.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);

    resultingValue = resultingStrategy.shouldBreak(mock(Task.class), mock(ExecutionContext.class));

    assertThat(resultingValue, equalTo(false));

    // test 3

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy3.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy4.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);

    resultingValue = resultingStrategy.shouldBreak(mock(Task.class), mock(ExecutionContext.class));

    assertThat(resultingValue, equalTo(false));


    // test 4

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy3.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy4.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);

    resultingValue = resultingStrategy.shouldBreak(mock(Task.class), mock(ExecutionContext.class));

    assertThat(resultingValue, equalTo(false));

    // test 5

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy3.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy4.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);

    resultingValue = resultingStrategy.shouldBreak(mock(Task.class), mock(ExecutionContext.class));

    assertThat(resultingValue, equalTo(true));

    // test 6

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy3.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy4.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);

    resultingValue = resultingStrategy.shouldBreak(mock(Task.class), mock(ExecutionContext.class));

    assertThat(resultingValue, equalTo(true));

    // test 7

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy3.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy4.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);

    resultingValue = resultingStrategy.shouldBreak(mock(Task.class), mock(ExecutionContext.class));

    assertThat(resultingValue, equalTo(true));


  }

  @Test
  public void shouldWorkCorrectlyWithNewDslDefinedAsInstance() throws Exception {
    // given
    class MyTestDsl extends SimpleStrategyDsl {
      public BreakStrategy firstOf(BreakStrategy... strategies) {
        if (strategies.length == 0) {
          throw new IllegalArgumentException("Should be at least one strategy method defined");
        }

        return strategies[0];
      }

    }

    BreakStrategy strategy1 = mock(BreakStrategy.class);
    BreakStrategy strategy2 = mock(BreakStrategy.class);
    BreakStrategy strategy3 = mock(BreakStrategy.class);
    BreakStrategy strategy4 = mock(BreakStrategy.class);

    when(strategy1.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);
    when(strategy2.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy3.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);
    when(strategy4.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);

    // when
    final BreakStrategy breakStrategy = new MyTestDsl().firstOf(strategy1, strategy2, strategy3, strategy4);

    // then
    assertThat(breakStrategy, equalTo(strategy1));
    assertThat(breakStrategy.shouldBreak(mock(Task.class), mock(ExecutionContext.class)), equalTo(false));

  }


}
