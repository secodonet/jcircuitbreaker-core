package net.secodo.jcircuitbreaker.breaker.impl;

import net.secodo.jcircuitbreaker.breaker.CircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakhandler.impl.NoActionHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;
import net.secodo.jcircuitbreaker.task.VoidTask;
import org.junit.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class DefaultCircuitBreakerVoidMethodsTest {

  @Test
  public void shouldCorrectlyExecuteVoidMethod() throws TaskExecutionException {
    // given
    final CircuitBreaker circuitBreaker = new DefaultCircuitBreaker();
    final SomeInterfaceWithVoidMethod someObject = mock(SomeInterfaceWithVoidMethod.class);
    doAnswer(invocation -> 555).when(someObject).someMethod(anyInt());

    final VoidTask task = () -> someObject.someMethod(1);

    final BreakStrategy<Void> breakStrategy = mock(BreakStrategy.class);
    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false);

    final BreakHandler<Void> breakHandler = new NoActionHandler<>();

    // when
    circuitBreaker.execute(task, breakStrategy, breakHandler);

    // then
    verify(someObject).someMethod(1);


  }


  interface SomeInterfaceWithVoidMethod {
    void someMethod(Integer someParam);
  }


}