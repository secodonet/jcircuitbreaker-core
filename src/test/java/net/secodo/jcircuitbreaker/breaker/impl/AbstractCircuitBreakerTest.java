package net.secodo.jcircuitbreaker.breaker.impl;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNull.notNullValue;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import java.lang.reflect.Field;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import net.secodo.jcircuitbreaker.breaker.CircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutedTask;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.exception.CircuitBreakerException;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;

import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;


/**
 * NOTE: this really tests the AbstractCircuitBreaker -therefore maybe test should be renamed to
 * AbstractCircuitBreakerTest
 */
@SuppressWarnings("unchecked")
public class AbstractCircuitBreakerTest {
  @Test
  public void shouldStopMethodExecutionIfBreakerDecidesToBreak() throws TaskExecutionException {
    // given
    final long returnValue = 99999L;
    final long fallbackReturnValue = 33333L;
    final String paramValue1 = "VALUE IS HERE";
    final Integer paramValue2 = 10;

    final BreakStrategy<Long> breakStrategy = mock(BreakStrategy.class);
    final List<?> userData = mock(List.class);
    final BreakHandler<Long> breakHandler = mock(BreakHandler.class);
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);

    Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true); // break will happen
    when(someTestObject.someMethod(paramValue1, paramValue2)).thenReturn(returnValue);
    when(
      breakHandler.onBreak(any(ContextAwareCircuitBreaker.class),
        any(Task.class),
        any(BreakStrategy.class),
        any(ExecutionContext.class))).thenReturn(
      fallbackReturnValue);

    // when
    final Long methodReturnValue =
      new SomeAbstractCircuitBreakerForTest<Long>().execute(methodCall,
        breakStrategy,
        breakHandler,
        userData);

    final Long methodReturnValueFromStatic =
      new SomeAbstractCircuitBreakerForTest<Long>().execute(methodCall,
        breakStrategy,
        breakHandler,
        userData); // also test static call of JCircuitBreaker


    // then
    verify(someTestObject, never()).someMethod(anyString(), anyInt());
    verify(breakHandler, times(2)).onBreak(any(ContextAwareCircuitBreaker.class),
      any(Task.class),
      any(BreakStrategy.class),
      any(ExecutionContext.class)); // +1 for normal call, + 1 for static call

    assertThat(methodReturnValue, equalTo(fallbackReturnValue));
    assertThat(methodReturnValueFromStatic, equalTo(methodReturnValue)); // static call verification

  }

  @Test
  public void shouldContinueAndExecuteMethodIfBreakerDecidesNotToBreak() throws TaskExecutionException {
    // given
    final Long returnValue = 9999L;
    final long fallbackReturnValue = 3333L;
    final String paramValue1 = "VALUE IS HERE";
    final Integer paramValue2 = 10;

    final BreakStrategy<Long> breakStrategy = mock(BreakStrategy.class);
    final List<?> userData = mock(List.class);
    final BreakHandler<Long> breakHandler = mock(BreakHandler.class);
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);

    Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false); // no break
    when(someTestObject.someMethod(paramValue1, paramValue2)).thenReturn(returnValue);
    when(
      breakHandler.onBreak(any(ContextAwareCircuitBreaker.class),
        any(Task.class),
        any(BreakStrategy.class),
        any(ExecutionContext.class))).thenReturn(
      fallbackReturnValue);

    // when
    final Long methodReturnValue =
      new SomeAbstractCircuitBreakerForTest<Long>().execute(methodCall,
        breakStrategy,
        breakHandler,
        userData);

    final Long methodReturnValueFromStatic =
      new SomeAbstractCircuitBreakerForTest<Long>().execute(methodCall,
        breakStrategy,
        breakHandler,
        userData); // also test static JCircuitBreaker


    // then
    verify(someTestObject, times(2)).someMethod(anyString(), anyInt()); // 1 for normal call + 1 for static call
    verify(breakHandler, never()).onBreak(any(ContextAwareCircuitBreaker.class),
      any(Task.class),
      any(BreakStrategy.class),
      any(ExecutionContext.class));

    assertThat(methodReturnValue, equalTo(returnValue));
    assertThat(methodReturnValueFromStatic, equalTo(methodReturnValue)); // static call verification

  }

  @Test
  public void shouldThrowTaskExecutionExceptionIfTaskThrowsException() {
    final String paramValue1 = "VALUE IS HERE";
    final Integer paramValue2 = 10;

    final BreakHandler<Long> breakHandler = mock(BreakHandler.class);

    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);
    when(someTestObject.someMethod(paramValue1, paramValue2)).thenThrow(IOException.class); // any exception

    final Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    final BreakStrategy<Long> breakStrategy = mock(BreakStrategy.class);
    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false); // no break

    final SomeAbstractCircuitBreakerForTest<Long> defaultCircuitBreaker = new SomeAbstractCircuitBreakerForTest<>();

    // when


    try {
      defaultCircuitBreaker.execute(methodCall, breakStrategy, breakHandler);
      fail("TaskExecution exception should have been thrown");
    } catch (TaskExecutionException e) {
      // then
      assertThat(e.getCause(), notNullValue());
      assertThat(IOException.class.isInstance(e.getCause()), equalTo(true));
      assertThat(e.getTaskException(), notNullValue());
      assertThat(IOException.class.isInstance(e.getTaskException()), equalTo(true));
    }

  }

  @Test
  public void shouldThrowCircuitBreakerExceptionWhenUnexpectedExceptionOccurredWhenProcessingTheTask()
                                                                                              throws Exception {
    final String paramValue1 = "VALUE IS HERE";
    final Integer paramValue2 = 10;

    final BreakHandler<Long> breakHandler = mock(BreakHandler.class);

    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);
    when(someTestObject.someMethod(paramValue1, paramValue2)).thenThrow(IOException.class); // any exception

    final Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    final BreakStrategy<Long> breakStrategy = mock(BreakStrategy.class);
    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false); // no break

    final ConcurrentHashMap<String, ExecutedTask<Long>> hashMapMock = mock(ConcurrentHashMap.class);
    final Field tasksInProgressField = AbstractCircuitBreaker.class.getDeclaredField("tasksInProgress");
    tasksInProgressField.setAccessible(true);

    final SomeAbstractCircuitBreakerForTest<Long> circuitBreaker = new SomeAbstractCircuitBreakerForTest<>();
    tasksInProgressField.set(circuitBreaker, hashMapMock);

    when(hashMapMock.put(any(String.class), any(ExecutedTask.class))).thenThrow(Exception.class);

    // when
    try {
      circuitBreaker.execute(methodCall, breakStrategy, breakHandler);
      fail(CircuitBreakerException.class.getSimpleName() + " should have been thrown");
    } catch (CircuitBreakerException e) {
      // ok

    }

    verify(hashMapMock, times(1)).put(any(String.class), any(ExecutedTask.class));

  }

  @Test
  public void shouldThrowCircuitBreakerExceptionWhenTaskHashcodeResultsInException() throws Exception {
    final String paramValue1 = "VALUE IS HERE";
    final Integer paramValue2 = 10;

    final BreakHandler<Long> breakHandler = mock(BreakHandler.class);

    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);
    when(someTestObject.someMethod(paramValue1, paramValue2)).thenThrow(IOException.class); // any exception

    AtomicBoolean hashcodeCalled = new AtomicBoolean(false);

    final Task<Long> methodCall = new Task<Long>() {
      @Override
      public Long execute() throws Exception {
        return null;
      }

      @Override
      public int hashCode() {
        hashcodeCalled.set(true);
        throw new UnsupportedOperationException("abc");
      }
    };

    final BreakStrategy<Long> breakStrategy = mock(BreakStrategy.class);
    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false); // no break


    final SomeAbstractCircuitBreakerForTest<Long> circuitBreaker = new SomeAbstractCircuitBreakerForTest<>();

    // when
    try {
      circuitBreaker.execute(methodCall, breakStrategy, breakHandler);

      // then

      fail(CircuitBreakerException.class.getSimpleName() + " should have been thrown");
    } catch (CircuitBreakerException e) {
      // ok
      assertThat(e.getCause(), notNullValue());
      assertThat(e.getCause(), instanceOf(UnsupportedOperationException.class));
    }

    assertThat(hashcodeCalled.get(), equalTo(true));

  }

  @Test
  public void shouldReuseExecutionContextWhenBreakHandlerCallsExecuteMethodOfCircuitBreaker() throws Exception {
    // given
    HashMap<Integer, Integer> executionContextCounter = new HashMap<>();

    BreakHandler<AtomicInteger> breakHandler = new BreakHandler<AtomicInteger>() {
      @Override
      public AtomicInteger onBreak(ContextAwareCircuitBreaker<AtomicInteger> circuitBreaker,
                                   Task<AtomicInteger> task, BreakStrategy<AtomicInteger> breakStrategy,
                                   ExecutionContext<AtomicInteger> executionContext) throws TaskExecutionException,
                                                                                            CircuitBreakerException,
                                                                                            BreakHandlerException {
        final int hashCode = executionContext.hashCode();

        if (!executionContextCounter.containsKey(hashCode)) {
          executionContextCounter.put(hashCode, 0);
        }

        executionContextCounter.put(hashCode, executionContextCounter.get(hashCode) + 1);
        return circuitBreaker.executeInContext(task, breakStrategy, this, executionContext);
      }
    };


    BreakStrategy<AtomicInteger> breakStrategy = mock(BreakStrategy.class);
    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true)
    .thenReturn(true)
    .thenReturn(true)
    .thenReturn(false);

    final Task<AtomicInteger> task = mock(Task.class);
    when(task.execute()).thenReturn(new AtomicInteger(999));

    final CircuitBreaker<AtomicInteger> breaker = new SomeAbstractCircuitBreakerForTest<>();

    // when
    final AtomicInteger result = breaker.execute(task, breakStrategy, breakHandler);

    // then
    verify(breakStrategy, times(4)).shouldBreak(any(Task.class), any(ExecutionContext.class));
    assertThat(executionContextCounter.size(), equalTo(1));
    assertThat(executionContextCounter.entrySet().iterator().next().getValue(), equalTo(3));

    assertThat(result.get(), equalTo(999));


  }


  class SomeTestClassWithSomeMethod {
    SomeTestClassWithSomeMethod() {
    }

    public Long someMethod(String paramValue1, Integer paramValue2) {
      return (long) 1;
    }


  }

  class SomeAbstractCircuitBreakerForTest<R> extends AbstractCircuitBreaker<R> {
  }


}
