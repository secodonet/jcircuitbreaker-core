package net.secodo.jcircuitbreaker.breaker.impl;

import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;
import net.secodo.jcircuitbreaker.breaker.CircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutedTask;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;


@SuppressWarnings("unchecked")
public class DefaultCircuitBreakerConcurrencyTest {
  @Test(timeout = 20000L)
  public void shouldCorrectlyHandleNumberOfTasksInProgress() throws Exception {
    // given
    final DefaultCircuitBreaker circuitBreaker = new DefaultCircuitBreaker();

    final Field tasksInProgressField = AbstractCircuitBreaker.class.getDeclaredField("tasksInProgress");
    tasksInProgressField.setAccessible(true);

    final Map<String, ExecutedTask> tasksInProgress = (Map<String, ExecutedTask>) tasksInProgressField.get(
      circuitBreaker);

    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final BreakHandler<Long> breakHandler = mock(BreakHandler.class);


    //    final SomeTestClassWithLongRunningMethod someObject = new SomeTestClassWithLongRunningMethod();
    //    Task<Long> methodCall = () -> someObject.longRunMethod("222", 333);

    final int numberOfThreads = 10;
    Stack<Thread> concurrentThreadsStack = new Stack<>();
    Stack<SomeTestClassWithLongRunningMethod> objectsUnderTestStack = new Stack<>();


    final CountDownLatch latch = new CountDownLatch(numberOfThreads);


    IntStream.range(0, numberOfThreads).forEach(i -> {
      final SomeTestClassWithLongRunningMethod someObject = new SomeTestClassWithLongRunningMethod();
      final Task<Long> methodCall = () -> someObject.longRunMethod("222", 333);

      final TaskWithFixedHashcode<Long> taskWithFixedHashcode = new TaskWithFixedHashcode(methodCall); // make sure that fixed hashcode does not all tasks to

      // override each other when CircuitBreaker is running


      Thread thread = new Thread(() -> {
        try {
          circuitBreaker.execute(taskWithFixedHashcode, breakStrategy, breakHandler);
        } catch (TaskExecutionException e) {
          throw new RuntimeException("Exception while running method on thread no: " + i, e);
        }
      });

      concurrentThreadsStack.push(thread);
      objectsUnderTestStack.push(someObject);

    });

    concurrentThreadsStack.forEach(Thread::start);
    concurrentThreadsStack.forEach(t -> waitUntilAllTasksAreInProgress(tasksInProgress, numberOfThreads));


    assertThat(tasksInProgress.size(), equalTo(numberOfThreads));

    Thread annihilator = new Thread(() -> {
      int numberOfTasksInProgress = tasksInProgress.size();

      while (!objectsUnderTestStack.empty()) {
        try {
          final SomeTestClassWithLongRunningMethod currentObject = objectsUnderTestStack.pop();
          final Thread threadOfCurrentObject = concurrentThreadsStack.pop();

          currentObject.annihilate();
          numberOfTasksInProgress--;

          waitUntilThreadIsDead(threadOfCurrentObject);

          assertThat(tasksInProgress.size(), equalTo(numberOfTasksInProgress));

        } finally {
          latch.countDown();

        }
      }

    });

    annihilator.start();


    latch.await(10, TimeUnit.MINUTES);
  }

  private void waitUntilAllTasksAreInProgress(Map<String, ExecutedTask> tasksInProgress, int expectedNumberOfTasks) {
    final int maxLoopIterations = 200;
    int currentLoopIteration = 0;
    final int sleepTimeMilis = 10;


    while (tasksInProgress.size() != expectedNumberOfTasks) {
      try {
        Thread.sleep(sleepTimeMilis);
      } catch (InterruptedException e) {
        // ok to continue
      }

      currentLoopIteration++;

      if (currentLoopIteration == maxLoopIterations) {
        throw new RuntimeException(
          "After: " + (sleepTimeMilis * maxLoopIterations) + " miliseconds number of tasks in progress within " +
          CircuitBreaker.class.getSimpleName() + " was: " +
          tasksInProgress.size() + ", but expected " + expectedNumberOfTasks +
          ". It may indicate a problem in calculating Key for internal " +
          "map storing tasks in progress.");
      }

    }
  }

  private void waitUntilThreadIsDead(Thread thread) {
    while (thread.isAlive()) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        // ok to continue
      }
    }
  }

  class SomeTestClassWithLongRunningMethod extends Thread {
    private boolean shouldRun = true;
    private boolean started = false;

    SomeTestClassWithLongRunningMethod() {
    }

    public Long longRunMethod(String paramValue1, Integer paramValue2) {
      started = true;

      while (shouldRun) {
        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
          // interrupted
        }
      }

      return (long) 1;
    }

    public void annihilate() {
      shouldRun = false;
    }

    public boolean isStarted() {
      return started;
    }
  }

  class TaskWithFixedHashcode<R> implements Task<R> {
    private final Task<R> methodCall;

    TaskWithFixedHashcode(Task<R> c) {
      this.methodCall = c;
    }


    @Override
    public R execute() throws Exception {
      return methodCall.execute();
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if ((o == null) || (getClass() != o.getClass())) {
        return false;
      }

      TaskWithFixedHashcode<?> that = (TaskWithFixedHashcode<?>) o;
      return Objects.equals(methodCall, that.methodCall);
    }

    @Override
    public int hashCode() {
      return 5;
    }
  }

}
