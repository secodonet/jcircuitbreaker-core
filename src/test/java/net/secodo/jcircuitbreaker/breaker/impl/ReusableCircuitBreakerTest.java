package net.secodo.jcircuitbreaker.breaker.impl;

import static org.hamcrest.core.IsEqual.equalTo;

import static org.junit.Assert.assertThat;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandlerFactory;
import net.secodo.jcircuitbreaker.breakhandler.OnePerExecutionHandlerFactory;
import net.secodo.jcircuitbreaker.breakhandler.impl.StatefulRetryHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;

import org.junit.Test;


@SuppressWarnings({ "unchecked" })
public class ReusableCircuitBreakerTest {
  @Test
  public void shouldConstructWorkingCircuitBreakerUsingBuilder() throws TaskExecutionException {
    // given
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);
    when(someTestObject.someMethod(anyString(), anyInt())).thenReturn(7777L);

    final Task<Long> methodCall = () -> someTestObject.someMethod("asdsas", 13213);

    abstract class Cpu {
      abstract int getCpuLoad();
    }

    final Cpu cpu = mock(Cpu.class);

    ReusableCircuitBreaker<Long> breaker = ReusableCircuitBreaker.builder()
      .withBreakStrategy((task, executionContext) -> cpu.getCpuLoad() > 80)
      .withBreakHandler((circuitBreaker, task, breakStrategy, executionContext) -> -1L).build();

    when(cpu.getCpuLoad()).thenReturn(60).thenReturn(81);

    // when
    Long returnValue = breaker.execute(methodCall);

    // then
    assertThat(returnValue, equalTo(7777L)); // first time CPU was under 80% so the method was executed

    // when
    returnValue = breaker.execute(methodCall);

    // then
    assertThat(returnValue, equalTo(-1L)); // second time CPU was over 80% so the method was no executed and value returned by break handler was used


  }

  @Test
  public void shouldConstructWorkingCircuitBreakerUsingCheckedBuilder() throws TaskExecutionException {
    // given
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);
    when(someTestObject.someMethod(anyString(), anyInt())).thenReturn(7777L);

    final Task<Long> methodCall = () -> someTestObject.someMethod("asdsas", 13213);

    abstract class Cpu {
      abstract int getCpuLoad();
    }

    final Cpu cpu = mock(Cpu.class);

    ReusableCircuitBreaker<Long> breaker = ReusableCircuitBreaker.<Long>builderP()
      .withBreakStrategy((task,executionContext) -> cpu.getCpuLoad() > 80)
      .withBreakHandler((circuitBreaker, task, breakStrategy, executionContext) -> -1L)
      .build();

    when(cpu.getCpuLoad()).thenReturn(60).thenReturn(81);

    // when
    Long returnValue = breaker.execute(methodCall);

    // then
    assertThat(returnValue, equalTo(7777L)); // first time CPU was under 80% so the method was executed

    // when
    returnValue = breaker.execute(methodCall);

    // then
    assertThat(returnValue, equalTo(-1L)); // second time CPU was over 80% so the method was no executed and value returned by break handler was used


  }

  @Test
  public void shouldStopMethodExecutionIfBreakerDecidesToBreak() throws TaskExecutionException {
    // given
    final long returnValue = 99999L;
    final long fallbackReturnValue = 33333L;
    final String paramValue1 = "VALUE IS HERE";
    final Integer paramValue2 = 10;

    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final List<?> userData = mock(List.class);
    final BreakHandler<Long> breakHandler = mock(BreakHandler.class);
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);

    Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true); // break will happen
    when(someTestObject.someMethod(paramValue1, paramValue2)).thenReturn(returnValue);
    when(
      breakHandler.onBreak(any(ContextAwareCircuitBreaker.class),
        any(Task.class),
        any(BreakStrategy.class),
        any(ExecutionContext.class))).thenReturn(
      fallbackReturnValue);

    // when
    final ReusableCircuitBreaker<Long> genericCBTest = new ReusableCircuitBreaker<>(breakStrategy,
      breakHandler);
    final Long methodReturnValue = genericCBTest.execute(methodCall, userData);


    // then
    verify(someTestObject, never()).someMethod(anyString(), anyInt());
    verify(breakHandler, times(1)).onBreak(any(ContextAwareCircuitBreaker.class),
      any(Task.class),
      any(BreakStrategy.class),
      any(ExecutionContext.class));

    assertThat(methodReturnValue, equalTo(fallbackReturnValue));

  }

  @Test
  public void shouldContinueAndExecuteMethodIfBreakerDecidesNotToBreak() throws TaskExecutionException {
    // given
    final Long returnValue = 9999L;
    final long fallbackReturnValue = 3333L;
    final String paramValue1 = "VALUE IS HERE";
    final Integer paramValue2 = 10;

    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final List<?> userData = mock(List.class);
    final BreakHandler<Long> breakHandler = mock(BreakHandler.class);
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);

    Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(false); // no break
    when(someTestObject.someMethod(paramValue1, paramValue2)).thenReturn(returnValue);
    when(
      breakHandler.onBreak(any(ContextAwareCircuitBreaker.class),
        any(Task.class),
        any(BreakStrategy.class),
        any(ExecutionContext.class))).thenReturn(
      fallbackReturnValue);

    // when
    final ReusableCircuitBreaker<Long> genericCB = new ReusableCircuitBreaker<>(breakStrategy,
      breakHandler);
    final Long methodReturnValue = genericCB.execute(methodCall, userData);


    // then
    verify(someTestObject, times(1)).someMethod(anyString(), anyInt());
    verify(breakHandler, never()).onBreak(any(ContextAwareCircuitBreaker.class),
      any(Task.class),
      any(BreakStrategy.class),
      any(ExecutionContext.class));

    assertThat(methodReturnValue, equalTo(returnValue));
  }

  @Test
  public void shouldNotShareStateBetweenExecutionsWhenOnePerRequestHandlerFactoryIsUsedWithNotReusableBreakHandler()
    throws TaskExecutionException {
    // given
    final Long returnValue = 9999L;
    final long fallbackReturnValue = 3333L;
    final String paramValue1 = "VALUE IS HERE";
    final Integer paramValue2 = 10;

    final BreakStrategy breakStrategy = mock(BreakStrategy.class);

    // prepare break handler which is not reusable
    LinkedHashMap<Integer, Integer> invocationsCounted = new LinkedHashMap<>();


    class NotReusableBreakHandler implements BreakHandler<Long> {
      @Override
      public Long onBreak(ContextAwareCircuitBreaker<Long> circuitBreaker, Task<Long> task,
                          BreakStrategy<Long> breakStrategy, ExecutionContext<Long> executionContext)
                   throws TaskExecutionException {
        int thisObject = this.hashCode();

        if (!invocationsCounted.containsKey(thisObject)) {
          invocationsCounted.put(thisObject, 1);
        } else {
          int count = invocationsCounted.get(thisObject);
          invocationsCounted.put(thisObject, count + 1);
        }


        return fallbackReturnValue;
      }
    }


    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);
    Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true).thenReturn(true).thenReturn(true);

    // factory below (each invocation should create new handler) instead of using the same instance of break handler
    final ReusableCircuitBreaker<Long> breaker = new ReusableCircuitBreaker<>(breakStrategy,
      (OnePerExecutionHandlerFactory<Long>) (task, executionContext) -> new NotReusableBreakHandler());

    // when
    Long methodReturnValue = breaker.execute(methodCall);


    // then
    assertThat(invocationsCounted.size(), equalTo(1)); // only one execution of CircuitBreaker took place
    assertThat(invocationsCounted.entrySet().iterator().next().getValue(),
      equalTo(1)); // break handler executed only once

    verifyZeroInteractions(someTestObject);

    assertThat(methodReturnValue, equalTo(fallbackReturnValue));

    // NOW what if another invocation happens

    // when
    methodReturnValue = breaker.execute(methodCall);

    // then
    assertThat(invocationsCounted.size(), equalTo(2)); // now two different executions took place

    final Iterator<Map.Entry<Integer, Integer>> invocationMapIt = invocationsCounted.entrySet().iterator();
    assertThat(invocationMapIt.next().getValue(),
      equalTo(1)); // first execution called 1 time to break handler (no change here)
    assertThat(invocationMapIt.next().getValue(), equalTo(1)); // second execution called 1 times break handler

    verifyZeroInteractions(someTestObject);

    assertThat(methodReturnValue, equalTo(fallbackReturnValue));

  }

  @Test
  public void shouldNotShareStateBetweenExecutionsWhenUsingOnePerExecutionHandlerFactoryWithStatefulRetryHandler()
    throws TaskExecutionException {
    // given
    final Long returnValue = 9999L;
    final String paramValue1 = "VALUE IS HERE";
    final Integer paramValue2 = 10;

    // prepare counting retry handler which is not reusable
    LinkedHashMap<Integer, Integer> invocationsCounted = new LinkedHashMap<>();

    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);
    when(someTestObject.someMethod(paramValue1, paramValue2)).thenReturn(returnValue);

    Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    class MyStatefulRetryHandler extends StatefulRetryHandler<Long> {
      public MyStatefulRetryHandler(int maxNumberOfRetries) {
        super(maxNumberOfRetries);
      }

      @Override
      protected void onRetry(int currentRetryAttempt, Task<Long> task, ExecutionContext<Long> executionContext) {
        int thisObject = this.hashCode();

        if (!invocationsCounted.containsKey(thisObject)) {
          invocationsCounted.put(thisObject, 1);
        } else {
          int count = invocationsCounted.get(thisObject);
          invocationsCounted.put(thisObject, count + 1);
        }
      }
    }

    BreakHandlerFactory<Long> breakHandlerFactory = (OnePerExecutionHandlerFactory<Long>) (task, executionContext) ->
      new MyStatefulRetryHandler(10);

    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true)
    .thenReturn(true)
    .thenReturn(true)
    .thenReturn(false) // three invocations of breakHandler (3 x thenReturn=true) until the actual method gets executed (thenReturn=false)
    .thenReturn(true)
    .thenReturn(true)
    .thenReturn(false); // for second execution of circuit breaker there are 2 invocations of breakHandler (2 x thenReturn=true) until the actual method gets executed

    final ReusableCircuitBreaker<Long> breaker = new ReusableCircuitBreaker<>(breakStrategy,
      breakHandlerFactory); // factory method (each invocation should create new handler) used instead of using the same

    // when
    Long methodReturnValue = breaker.execute(methodCall);


    // then
    assertThat(invocationsCounted.size(), equalTo(1)); // only one execution of CircuitBreaker took place so only one instance of MyStatefulRetryHandler should have been created
    assertThat(invocationsCounted.entrySet().iterator().next().getValue(), equalTo(3)); // create instance should have been called 3 times...

    verify(someTestObject, times(1)).someMethod(any(String.class), any(Integer.class)); // ... until target-method was executed

    assertThat(methodReturnValue, equalTo(returnValue));

    // NOW what if another invocation happens

    // when
    methodReturnValue = breaker.execute(methodCall);

    // then
    assertThat(invocationsCounted.size(), equalTo(2)); // now two different executions took place

    final Iterator<Map.Entry<Integer, Integer>> invocationMapIt = invocationsCounted.entrySet().iterator();
    assertThat(invocationMapIt.next().getValue(), equalTo(3)); // first execution called 3 times break handler (no change here)
    assertThat(invocationMapIt.next().getValue(), equalTo(2)); // second execution called 2 times break handler

    verify(someTestObject, times(2)).someMethod(any(String.class), any(Integer.class)); // two executions were made

    assertThat(methodReturnValue, equalTo(returnValue));

  }


  class SomeTestClassWithSomeMethod {
    SomeTestClassWithSomeMethod() {
    }

    public Long someMethod(String paramValue1, Integer paramValue2) {
      return (long) 1;
    }


  }

}
