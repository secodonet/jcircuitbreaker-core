package net.secodo.jcircuitbreaker.breaker.builder.impl;

import net.secodo.jcircuitbreaker.breaker.builder.BuilderValidationException;
import net.secodo.jcircuitbreaker.breaker.impl.ReusableCircuitBreaker;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;


@SuppressWarnings("unchecked")
public class ReusableCircuitBreakerBuilderTest {
  @Test(expected = BuilderValidationException.class)
  public void shouldResultInValidationExceptionWhenBuilderDoesNotDefineAnyProperties() throws Exception {
    new ReusableCircuitBreakerBuilder<Long>().build();
  }

  @Test(expected = BuilderValidationException.class)
  public void shouldResultInExceptionWhenNoBreakStrategyIsGiven() throws Exception {
    new ReusableCircuitBreakerBuilder<Long>().withBreakHandler(mock(BreakHandler.class)).build();
  }

  @Test(expected = BuilderValidationException.class)
  public void shouldResultInExceptionWhenNoBreakHandlerIsGiven() throws Exception {
    new ReusableCircuitBreakerBuilder<Long>().withBreakStrategy(mock(BreakStrategy.class)).build();
  }

  @Test
  public void shouldCreateCircuitBreakerWithBreakStrategyAndBreakHandler() throws Exception {
    // given
    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final BreakHandler breakHandler = mock(BreakHandler.class);

    // when
    final ReusableCircuitBreaker breaker = new ReusableCircuitBreakerBuilder<Long>().withBreakStrategy(breakStrategy)
      .withBreakHandler(breakHandler)
      .build();

    // then
    assertThat(getBreakStrategy(breaker), equalTo(breakStrategy));
    assertThat(getBreakHandler(breaker), equalTo(breakHandler));

  }


  private static <R> BreakStrategy<R> getBreakStrategy(ReusableCircuitBreaker<R> circuitBreaker) throws
    NoSuchFieldException, IllegalAccessException {

    final Field field = ReusableCircuitBreaker.class.getDeclaredField("breakStrategy");
    field.setAccessible(true);
    return (BreakStrategy) field.get(circuitBreaker);
  }

  private static <R> BreakHandler<R> getBreakHandler(ReusableCircuitBreaker<R> circuitBreaker) throws
    NoSuchFieldException, IllegalAccessException {

    final Field field = ReusableCircuitBreaker.class.getDeclaredField("breakHandler");
    field.setAccessible(true);
    return (BreakHandler) field.get(circuitBreaker);
  }

}
