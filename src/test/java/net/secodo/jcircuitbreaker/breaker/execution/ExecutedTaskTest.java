package net.secodo.jcircuitbreaker.breaker.execution;

import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;


public class ExecutedTaskTest {
  @Test
  public void testEquality() throws Exception { // important so that two tasks does not overwrite
    EqualsVerifier.forClass(ExecutedTask.class).suppress(Warning.STRICT_INHERITANCE).verify();
  }

  @Test
  public void gettersTest() throws Exception {
    Validator validator = ValidatorBuilder.create().with(new GetterTester()).build();

    validator.validate(PojoClassFactory.getPojoClass(ExecutedTask.class));
  }


}
