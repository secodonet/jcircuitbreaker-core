package net.secodo.jcircuitbreaker.breaker.execution.impl;

import static org.hamcrest.CoreMatchers.equalTo;

import static org.hamcrest.core.IsNull.nullValue;

import static org.junit.Assert.assertThat;

import java.util.concurrent.ConcurrentHashMap;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutedTask;

import org.junit.Test;


@SuppressWarnings("unchecked")
public class DefaultExecutionContextImplTest {
  @Test
  public void shouldReturnGivenDataToConsumer() throws Exception { // getters test really

    // given
    ConcurrentHashMap<String, ExecutedTask<Long>> tasksInProgress = new ConcurrentHashMap<>();
    final ExecutedTask<Long> task = new ExecutedTask<>(() -> new Long(1L), System.currentTimeMillis());
    tasksInProgress.put("asdsd", task);

    Object userData = new Object();

    DefaultExecutionContextImpl<Long> context = new DefaultExecutionContextImpl<>(tasksInProgress, userData);

    // then

    assertThat(context.getExecutionsInProgress().size(), equalTo(1));
    assertThat(context.getExecutionsInProgress().iterator().next(), equalTo(task));
    assertThat(context.getUserData(), equalTo(userData));

  }

  @Test
  public void shouldReturnAddedContextAttributes() throws Exception { // getters test really

    // given
    ConcurrentHashMap<String, ExecutedTask<Long>> tasksInProgress = new ConcurrentHashMap<>();
    final ExecutedTask<Long> task = new ExecutedTask<>(() -> new Long(1L), System.currentTimeMillis());
    tasksInProgress.put("asdsd", task);

    Long userData = 1L;

    DefaultExecutionContextImpl<Long> context = new DefaultExecutionContextImpl<>(tasksInProgress, userData);

    // when    tasksInProgress.put("asdsd", new ExecutedTask(() -> new Long(1L), System.currentTimeMillis()));

    context.setContextAttribute("testAtt1", 1L);
    context.setContextAttribute("testAtt2", "tests");

    // then

    assertThat(context.hasContextAttribute("testAtt1"), equalTo(true));
    assertThat(context.hasContextAttribute("testAtt2"), equalTo(true));
    assertThat(context.hasContextAttribute("testAtt3"), equalTo(false));
    assertThat(context.hasContextAttribute("as"), equalTo(false));
    assertThat(context.getContextAttribute("testAtt1"), equalTo(1L));
    assertThat(context.getContextAttribute("testAtt2"), equalTo("tests"));
    assertThat(context.getContextAttribute("asdasds"), equalTo(null));

  }

  @Test
  public void shouldReturnFalseOrNullWhenAccessingAttributesWithoutAddingAnyAttributeFirst() throws Exception {
    // given
    ConcurrentHashMap<String, ExecutedTask<Long>> tasksInProgress = new ConcurrentHashMap<>();
    final ExecutedTask<Long> task = new ExecutedTask<>(() -> new Long(1L), System.currentTimeMillis());
    tasksInProgress.put("asdsd", task);

    Long userData = 1L;

    DefaultExecutionContextImpl<Long> context = new DefaultExecutionContextImpl<>(tasksInProgress, userData);

    // then

    assertThat(context.hasContextAttribute("testAtt1"), equalTo(false));
    assertThat(context.hasContextAttribute("testAtt2"), equalTo(false));
    assertThat(context.hasContextAttribute("testAtt3"), equalTo(false));
    assertThat(context.hasContextAttribute("as"), equalTo(false));
    assertThat(context.getContextAttribute("testAtt1"), nullValue());
    assertThat(context.getContextAttribute("testAtt2"), nullValue());
    assertThat(context.getContextAttribute("asdasds"), nullValue());

  }


}
