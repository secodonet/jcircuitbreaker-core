package net.secodo.jcircuitbreaker.breakhandler;

import net.secodo.jcircuitbreaker.breaker.CircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;
import net.secodo.jcircuitbreaker.breakhandler.impl.ReturnStaticValueHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;
import java.util.concurrent.atomic.AtomicInteger;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;


public class BreakHandlerFactoryTest {
  @Test
  public void shouldThrowBreakHandlerExceptionInCaseMakeHandlerMethodReturnedNull() throws Exception {
    // given
    final BreakHandler<Byte> myBreakHandlerFactory = (BreakHandlerFactory<Byte>) (task, executionContext) -> null;

    // when
    try {
      myBreakHandlerFactory.onBreak(mock(ContextAwareCircuitBreaker.class),
        mock(Task.class),
        mock(BreakStrategy.class),
        mock(ExecutionContext.class));

      fail(BreakHandlerException.class.getSimpleName() + " should have been thrown when makeHandler returns null");
    } catch (BreakHandlerException e) {
      // ok
    }


    // then
  }

  @Test
  public void shouldReturnNewInstanceOfHandlerWhenMakeHandlerMethodReturnsNewInstance() throws Exception {
    // given
    AtomicInteger callsCounter = new AtomicInteger();


    final BreakHandler<Byte> myBreakHandlerFactory = (BreakHandlerFactory<Byte>) (task, executionContext) -> {
      // increased value will indicate that this is different counter then in previous call to this factory method
      callsCounter.incrementAndGet();
      return new ReturnStaticValueHandler<>((byte) callsCounter.intValue());
    };

    // when
    final Byte value1 = myBreakHandlerFactory.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      mock(ExecutionContext.class));

    final Byte value2 = myBreakHandlerFactory.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      mock(ExecutionContext.class));


    // then
    assertThat(value1, equalTo((byte) 1));
    assertThat(value2, equalTo((byte) 2));
  }

}
