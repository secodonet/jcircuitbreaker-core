package net.secodo.jcircuitbreaker.breakhandler;

import static org.hamcrest.CoreMatchers.equalTo;

import static org.junit.Assert.assertThat;

import static org.mockito.Mockito.mock;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breaker.execution.impl.DefaultExecutionContextImpl;
import net.secodo.jcircuitbreaker.breakhandler.impl.ReturnStaticValueHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;

import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;


public class OnePerExecutionHandlerFactoryTest {
  @Test
  public void shouldCreateAnUseOnlyOnInstanceOfBreakHandlerPerExecutionContext() throws Exception {
    // given
    ExecutionContext<Long> executionContext = new DefaultExecutionContextImpl<>(new ConcurrentHashMap<>(), null);

    AtomicInteger counter = new AtomicInteger();

    OnePerExecutionHandlerFactory<Long> breakHandler = ((task, context) -> {
        counter.incrementAndGet();
        return new ReturnStaticValueHandler<>(counter.longValue());
      });

    // when
    final Long value1 = breakHandler.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext);

    final Long value2 = breakHandler.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext);

    final Long value3 = breakHandler.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext);

    // then

    // the same static value handler should be returned all the time (because it
    // already in current context), so the value of first such handler is one as defined by "counter" variable
    assertThat(value1, equalTo(1L));
    assertThat(value2, equalTo(1L));
    assertThat(value3, equalTo(1L));


    // NOW TEST WITH NEW CONTEXT

    // given

    executionContext = new DefaultExecutionContextImpl<>(new ConcurrentHashMap<>(), null);

    // when
    final Long value4 = breakHandler.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext);

    final Long value5 = breakHandler.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext);

    final Long value6 = breakHandler.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext);

    // then

    // the same static value handler should be returned all the time (because it
    // already in current context), new handler should have been created for the first call because new execution
    // context was supplied - and because of this the value od  "counter" variable should have been increased to 2
    assertThat(value4, equalTo(2L));
    assertThat(value5, equalTo(2L));
    assertThat(value6, equalTo(2L));
  }

  @Test
  public void shouldUseDifferentContextNamesForEachImplementationOfOnePerExecutionHandlerFactory()
    throws TaskExecutionException, NoSuchFieldException, IllegalAccessException {
    // given
    class Factory1 implements OnePerExecutionHandlerFactory<String> {
      @Override
      public BreakHandler<String> createNewHandler(Task<String> task, ExecutionContext<String> executionContext) {
        return (circuitBreaker, task1, breakStrategy, executionContext1) -> "ABC";
      }
    }

    class Factory2 implements OnePerExecutionHandlerFactory<String> {
      @Override
      public BreakHandler<String> createNewHandler(Task<String> task, ExecutionContext<String> executionContext) {
        return (circuitBreaker, task1, breakStrategy, executionContext1) -> "XYZ";
      }
    }

    Factory1 breakHandlerFactory1 = new Factory1();
    Factory2 breakHandlerFactory2 = new Factory2();

    DefaultExecutionContextImpl<String> executionContext = new DefaultExecutionContextImpl<>(new ConcurrentHashMap<>(), null);

    // when - then
    breakHandlerFactory1.onBreak(mock(ContextAwareCircuitBreaker.class), mock(Task.class), mock(BreakStrategy.class),
      executionContext);

    assertThat(getContextAttributes(executionContext).size(), equalTo(1));

    // when - then
    breakHandlerFactory1.onBreak(mock(ContextAwareCircuitBreaker.class), mock(Task.class), mock(BreakStrategy.class),
      executionContext); // another call to breakHandlerFactory1 in the same executionContext should use the same name

    assertThat(getContextAttributes(executionContext).size(), equalTo(1));

    // ============= now for second break handler factory ========== //

    // when - then
    breakHandlerFactory2.onBreak(mock(ContextAwareCircuitBreaker.class), mock(Task.class), mock(BreakStrategy.class),
      executionContext); // call to another factory should use different execution context variable name...

    assertThat(getContextAttributes(executionContext).size(), equalTo(2)); // ... so there should be two attributes in context

    // when - then
    breakHandlerFactory2.onBreak(mock(ContextAwareCircuitBreaker.class), mock(Task.class), mock(BreakStrategy.class),
      executionContext);

    assertThat(getContextAttributes(executionContext).size(), equalTo(2)); // no change.. still two factories


  }

  private Map<String, Object> getContextAttributes(DefaultExecutionContextImpl defaultExecutionContext)
    throws NoSuchFieldException, IllegalAccessException {

    Field contextAttributesField = DefaultExecutionContextImpl.class.getDeclaredField("contextAttributes");
    contextAttributesField.setAccessible(true);
    return (Map<String, Object>) contextAttributesField.get(defaultExecutionContext);
  }



}

