package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;


@SuppressWarnings("unchecked")
public class ReturnStaticValueHandlerTest {
  @Test
  public void shouldReturnDefinedStaticValueIfExecuted() throws TaskExecutionException {
    //given
    final int staticReturnValue = 5;
    final String paramValue1 = "44444444";
    final int paramValue2 = 222222;

    final ExecutionContext executionContext = mock(ExecutionContext.class);
    final ReturnStaticValueHandler<Integer> breakHandler = new ReturnStaticValueHandler<Integer>(staticReturnValue);
    final ContextAwareCircuitBreaker circuitBreaker = mock(ContextAwareCircuitBreaker.class);
    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);


    Task<Integer> task = () -> someTestObject.someMethod(paramValue1, paramValue2);

    //when
    final Integer returnedValue = breakHandler.onBreak(circuitBreaker, task, breakStrategy, executionContext);

    //then
    assertThat(returnedValue, equalTo(staticReturnValue));
  }

  class SomeTestClassWithSomeMethod {
    SomeTestClassWithSomeMethod() {
    }

    public Integer someMethod(String paramValue1, Integer paramValue2) {
      return 1;
    }


  }


}
