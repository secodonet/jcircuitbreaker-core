package net.secodo.jcircuitbreaker.breakhandler.impl;

public class BreakHandlerTestNotWorkingException extends Exception {
  public BreakHandlerTestNotWorkingException(String message) throws BreakHandlerTestInstantiationException {
    super(message);
    throw new BreakHandlerTestInstantiationException("Test exception - instantiation blocked");

  }
}
