package net.secodo.jcircuitbreaker.breakhandler.impl;

public class BreakHandlerTestInstantiationException extends Exception {
  public BreakHandlerTestInstantiationException(String message) {
    super(message);
  }
}
