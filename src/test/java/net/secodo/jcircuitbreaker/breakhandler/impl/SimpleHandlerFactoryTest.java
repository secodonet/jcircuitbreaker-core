package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.exception.CircuitBreakerException;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicLong;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class SimpleHandlerFactoryTest {

  @Test
  public void shouldCreateNewInstanceOfHandlerForEachInvocation() {
    // given
    SimpleHandlerFactory<Long> counterNextFactory = new SimpleHandlerFactory<>(CounterNextHandler.class);

    // when - then
    CounterNextHandler breakHandler1 = (CounterNextHandler) counterNextFactory.makeHandler(mock(Task.class), mock(ExecutionContext.class));
    assertThat(breakHandler1.getValue(), equalTo(1l));

    // when - then
    CounterNextHandler breakHandler2 = (CounterNextHandler) counterNextFactory.makeHandler(mock(Task.class), mock(ExecutionContext.class));
    assertThat(breakHandler2, not(equalTo(breakHandler1)));
    assertThat(breakHandler2.getValue(), equalTo(2l));

    // when - then
    CounterNextHandler breakHandler3 = (CounterNextHandler) counterNextFactory.makeHandler(mock(Task.class), mock(ExecutionContext.class));
    assertThat(breakHandler3, not(equalTo(breakHandler2)));
    assertThat(breakHandler3, not(equalTo(breakHandler1)));
    assertThat(breakHandler3.getValue(), equalTo(3l));
  }
  
  @Test
  public void shouldThrowBreakHandlerExceptionWhenPassedBreakHandlerClassDoesNotHaveAccessibleArgumentConstructor() {
    // given
    SimpleHandlerFactory<Long> counterNextFactory = new SimpleHandlerFactory<>(BreakHandlerWithoutDefaultConstructor.class);

    // when
    try {
      counterNextFactory.makeHandler(mock(Task.class), mock(ExecutionContext.class));
      fail(BreakHandlerException.class.getSimpleName() + " was expected");
    } catch (BreakHandlerException e) {
      // ok
    }

  }
  
  


}

class CounterNextHandler implements BreakHandler<Long> {
  private long value;

  private static AtomicLong counter = new AtomicLong(0);

  public CounterNextHandler() {
    this.value = counter.incrementAndGet();;
  }

  @Override
  public Long onBreak(ContextAwareCircuitBreaker<Long> circuitBreaker, Task<Long> task, BreakStrategy<Long> breakStrategy, ExecutionContext<Long> executionContext) throws TaskExecutionException, CircuitBreakerException, BreakHandlerException {
    return value;
  }

  public long getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    CounterNextHandler that = (CounterNextHandler) o;

    return value == that.value;
  }

  @Override
  public int hashCode() {
    return (int) (value ^ (value >>> 32));
  }
  
  
};
class BreakHandlerWithoutDefaultConstructor implements BreakHandler<Long> {
  public BreakHandlerWithoutDefaultConstructor(int A) {
  }

  @Override
  public Long onBreak(ContextAwareCircuitBreaker<Long> circuitBreaker, Task<Long> task, BreakStrategy<Long> breakStrategy, ExecutionContext<Long> executionContext) throws TaskExecutionException, CircuitBreakerException, BreakHandlerException {
    return 1L;
  }
  
};