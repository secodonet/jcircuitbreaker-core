package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;

class ExceptionThrowingHandlerTestMyException extends BreakHandlerException {

  public ExceptionThrowingHandlerTestMyException(String message) {
    super(message);
  }
}
    
    