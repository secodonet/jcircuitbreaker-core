package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutedTask;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class NoActionHandlerTest {

  @Test
  public void shouldReturnNullAndDoNotThrowBreakHandlerExceptionWhenUsingNoActionHandler() throws TaskExecutionException {
    // given
    
    // when
    String ret = new NoActionHandler<String>().onBreak(mock(ContextAwareCircuitBreaker.class), mock(Task.class), 
      mock(BreakStrategy.class), mock(ExecutionContext.class));

    // then
    assertThat(ret, is((String) null));

  }


}