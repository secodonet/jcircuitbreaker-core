package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;
import net.secodo.jcircuitbreaker.breaker.CircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SuppressWarnings("unchecked")
public class ExceptionThrowingHandlerTest {
  @Test
  public void shouldThrowBreakHandlerExceptionWhenJustMessageIsGivenAsConstructorParam() {
    final ExecutionContext<Short> executionContext = mock(ExecutionContext.class);
    final String exceptionMessage = "Expected test exception occurred while executing test task";
    final ExceptionThrowingHandler<Short> breakHandler = new ExceptionThrowingHandler(
      exceptionMessage);


    final ContextAwareCircuitBreaker<Short> circuitBreaker = mock(ContextAwareCircuitBreaker.class);
    final BreakStrategy<Short> breakStrategy = mock(BreakStrategy.class);
    final Task<Short> task = mock(Task.class);

    try {
      breakHandler.onBreak(circuitBreaker, task, breakStrategy, executionContext);
    } catch (BreakHandlerException e) {
      assertThat(e.getClass(), equalTo(BreakHandlerException.class));
      assertThat(e.getCause(), is(nullValue()));
      assertThat(e.getMessage(), equalTo(exceptionMessage));
    }
  }

  @Test
  public void shouldThrowBreakHandlerExceptionInCaseGivenExceptionClassConstructionResultsInException() {
    // given
    final ExecutionContext<Long> executionContext = mock(ExecutionContext.class);
    final ExceptionThrowingHandler<Long> breakHandler = new ExceptionThrowingHandler(
      BreakHandlerTestNotWorkingException.class,
      "Test exception occurred while executing test task");


    final ContextAwareCircuitBreaker<Long> circuitBreaker = mock(ContextAwareCircuitBreaker.class);
    final BreakStrategy<Long> breakStrategy = mock(BreakStrategy.class);
    final Task<Long> task = mock(Task.class);

    // when
    try {
      breakHandler.onBreak(circuitBreaker, task, breakStrategy, executionContext);
    } catch (Exception e) {
      // then
      assertThat(e.getClass(), equalTo(BreakHandlerException.class));
      assertThat(e.getCause(), is(notNullValue()));
      assertThat(e.getCause().getClass(), equalTo(InvocationTargetException.class)); // problem with constructing class via constructor
      assertThat(((InvocationTargetException) e.getCause()).getTargetException().getClass(),
        equalTo(BreakHandlerTestInstantiationException.class)); // problem with constructing class via constructor
    }
  }

  @Test
  public void shouldThrowBreakHandlerExceptionWithCustomMessageWhenMessageCallbackIsProvided() {
    // given
    final ExceptionThrowingHandler<Long> breakHandler = new ExceptionThrowingHandler(((task, execContext) -> "My " +
      "custom message " + execContext.getContextAttribute("messageId")
    ));

    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getContextAttribute("messageId")).thenReturn("A").thenReturn("B");

    final ContextAwareCircuitBreaker circuitBreaker = mock(ContextAwareCircuitBreaker.class);
    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final Task task = mock(Task.class);

    // when
    try {
      breakHandler.onBreak(circuitBreaker, task, breakStrategy, executionContext);
    } catch (Exception e) {
      // then
      assertThat(e.getClass(), equalTo(BreakHandlerException.class));
      assertThat(e.getCause(), is(nullValue()));
      assertThat(e.getMessage(), equalTo("My custom message A"));
    }

    // when
    try {
      breakHandler.onBreak(circuitBreaker, task, breakStrategy, executionContext);
    } catch (Exception e) {
      // then
      assertThat(e.getClass(), equalTo(BreakHandlerException.class));
      assertThat(e.getCause(), is(nullValue()));
      assertThat(e.getMessage(), equalTo("My custom message B"));
    }
  }


  @Test
  public void shouldThrowBreakHandlerExceptionWhenCustomExceptionIsNotInstanceOfBreakHandlerException() {
    // given

    // IOException is not instance of BreakHandlerExceptin
    final ExceptionThrowingHandler<Long> breakHandler = new ExceptionThrowingHandler(IOException.class,
      ((task, execContext) -> "My custom message " + execContext.getContextAttribute("messageId")
    ));

    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getContextAttribute("messageId")).thenReturn("A").thenReturn("B");

    final ContextAwareCircuitBreaker circuitBreaker = mock(ContextAwareCircuitBreaker.class);
    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final Task task = mock(Task.class);

    // when
    try {
      breakHandler.onBreak(circuitBreaker, task, breakStrategy, executionContext);
    } catch (Exception e) {
      // then
      assertThat(e.getClass(), equalTo(BreakHandlerException.class));
      assertThat(e.getCause(), is(notNullValue()));
      assertThat(e.getCause(), instanceOf(ClassCastException.class));
      assertThat(e.getMessage(), equalTo("Unable to throw custom exception of class: " + IOException.class +
        " and message: " + "My custom message A"));
    }

  }


  @Test
  public void shouldThrowCustomExceptionWhenDefinedAndUseCustomMessageWhenMessageCallbackIsProvided() {
    // given

 

    final ExceptionThrowingHandler<Long> breakHandler = new ExceptionThrowingHandler(ExceptionThrowingHandlerTestMyException.class,
      ((task, execContext) -> "My custom message " + execContext.getContextAttribute("messageId")
    ));

    final ExecutionContext executionContext = mock(ExecutionContext.class);
    when(executionContext.getContextAttribute("messageId")).thenReturn("A").thenReturn("B");

    final ContextAwareCircuitBreaker circuitBreaker = mock(ContextAwareCircuitBreaker.class);
    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final Task task = mock(Task.class);

    // when
    try {
      breakHandler.onBreak(circuitBreaker, task, breakStrategy, executionContext);
    } catch (Exception e) {
      // then
      assertThat(e.getClass(), equalTo(ExceptionThrowingHandlerTestMyException.class));
      assertThat(e.getCause(), is(nullValue()));
      assertThat(e.getMessage(), equalTo("My custom message A"));
    }

  }


}
