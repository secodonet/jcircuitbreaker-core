package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;
import net.secodo.jcircuitbreaker.exception.CircuitBreakerException;
import net.secodo.jcircuitbreaker.breaker.CircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breaker.impl.DefaultCircuitBreaker;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import java.lang.reflect.Field;
import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@SuppressWarnings("unchecked")
public class StatefulRetryHandlerTest {
  @Test
  public void shouldResultInRetryExceptionWhenBreakStrategyBlocksTaskExecutionAndNumberOfRetryAttemptsExceeds()
    throws TaskExecutionException, NoSuchFieldException, IllegalAccessException {
    // given
    final int maxNumberOfRetries = 3;
    final int wantedNumberOfTaskInvocations = maxNumberOfRetries + 1; //first one, and 3 more retries


    final StatefulRetryHandler<Long> retryHandler = new StatefulRetryHandler<>(maxNumberOfRetries);
    final CircuitBreaker<Long> circuitBreaker = new DefaultCircuitBreaker<>();

    final BreakStrategy<Long> breakStrategy = mock(BreakStrategy.class);
    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true); // strategy always does not allow execution

    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);

    Task<Long> methodCall = () -> someTestObject.someMethod("44444444", 222222);
    // when

    RetryHandlerException resultedException = null;

    try {
      circuitBreaker.execute(methodCall, breakStrategy, retryHandler, new Object());

      fail("Expected " + RetryHandlerException.class.getSimpleName() + " to occur, but no exception was thrown"); // because retry should fail
    } catch (RetryHandlerException e) {
      resultedException = e;
    }


    // then
    assertTrue(RetryHandlerException.class.isInstance(resultedException));
    verify(breakStrategy, times(wantedNumberOfTaskInvocations)).shouldBreak(any(Task.class), any(ExecutionContext.class));

    assertThat(getCurrentRetryAttempt(retryHandler), equalTo(maxNumberOfRetries));

  }

  @Test
  public void shouldRetryOnceInCaseOfStrategyDoesNotAllowTaskExecutionAndOnlyOneExtraInvocatinIsAllowedByRetryHandler()
    throws TaskExecutionException, NoSuchFieldException, IllegalAccessException {
    // given
    final int maxNumberOfRetries = 1;
    final int wantedNumberOfTaskInvocations = 2; //first one, and 1 more retry


    final StatefulRetryHandler<Long> retryHandler = new StatefulRetryHandler<>();
    final CircuitBreaker circuitBreaker = new DefaultCircuitBreaker();
    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);

    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true); // strategy always does not allow execution

    Task<Long> methodCall = () -> someTestObject.someMethod("44444444", 222222);

    // when
    RetryHandlerException resultedException = null;

    try {
      circuitBreaker.execute(methodCall, breakStrategy, retryHandler, new Object());

      fail("Expected " + CircuitBreakerException.class.getSimpleName() + " to occur, but no exception was thrown"); // because retry should fail
    } catch (RetryHandlerException e) {
      resultedException = e;
    }

    // then
    assertTrue(RetryHandlerException.class.isInstance(resultedException));
    verify(breakStrategy, times(wantedNumberOfTaskInvocations)).shouldBreak(any(Task.class), any(ExecutionContext.class));

    assertThat(getCurrentRetryAttempt(retryHandler), equalTo(maxNumberOfRetries));

  }

  @Test
  public void shouldRetryInCaseOfStrategyDoesNotAllowTaskExecutionThreeTimesButAllowFourthTime()
                                                                                        throws TaskExecutionException,
                                                                                               RetryHandlerException,
                                                                                               NoSuchFieldException,
                                                                                               IllegalAccessException {
    // given
    final int maxNumberOfRetries = 3;
    final int wantedNumberOfInvocations = maxNumberOfRetries + 1;
    final String paramValue1 = "44444444";
    final int paramValue2 = 222222;
    final Long returnValue = 88888L;


    final StatefulRetryHandler<Long> retryHandler = new StatefulRetryHandler<>(maxNumberOfRetries);
    final CircuitBreaker<Long> circuitBreaker = new DefaultCircuitBreaker<>();
    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);

    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true)
    .thenReturn(true)
    .thenReturn(true)
    .thenReturn(false); // strategy always does not allow execution
    when(someTestObject.someMethod(paramValue1, paramValue2)).thenReturn(returnValue);

    Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    // when
    // RetryHandlerException should not be thrown because the number of retry attempts does not excpeed allowed maximum
    final Long resultingReturnValue = circuitBreaker.execute(methodCall, breakStrategy, retryHandler, new Object());

    // then
    verify(breakStrategy, times(wantedNumberOfInvocations)).shouldBreak(any(Task.class), any(ExecutionContext.class));
    assertThat(getCurrentRetryAttempt(retryHandler), equalTo(3)); // because BreakStrategy returns true three times, and false 4th time

    verify(someTestObject, times(1)).someMethod(paramValue1, paramValue2);
    assertThat(resultingReturnValue, equalTo(returnValue));

  }

  @Test
  public void shouldCallOnRetryCallbackForEachRetryIfCallbackWasDefined() throws TaskExecutionException {
    // given
    final int maxNumberOfRetries = 3;
    final String paramValue1 = "44444444";
    final int paramValue2 = 222222;

    final RetryHandlerOnRetryCallback callback = mock(RetryHandlerOnRetryCallback.class);
    final StatefulRetryHandler<Long> retryHandler = new StatefulRetryHandler<>(maxNumberOfRetries, callback);

    final CircuitBreaker<Long> circuitBreaker = new DefaultCircuitBreaker();
    final BreakStrategy breakStrategy = mock(BreakStrategy.class);
    final SomeTestClassWithSomeMethod someTestObject = mock(SomeTestClassWithSomeMethod.class);

    when(breakStrategy.shouldBreak(any(Task.class), any(ExecutionContext.class))).thenReturn(true);

    Task<Long> methodCall = () -> someTestObject.someMethod(paramValue1, paramValue2);

    // when
    final Long resultingReturnValue;
    try {
      resultingReturnValue = circuitBreaker.execute(methodCall, breakStrategy, retryHandler, new Object());
      fail("RetryHandlerException should have been thrown"); // because breakStrategy always returns true, the retry handler should throw exception after it exceeded total number of retry attempts
    } catch (RetryHandlerException e) {
      // ok we should get here

    }

    // then
    verify(callback, times(maxNumberOfRetries)).onRetry(any(Integer.class), any(ExecutionContext.class));
    verify(breakStrategy, times(maxNumberOfRetries + 1)).shouldBreak(any(Task.class), any(ExecutionContext.class)); // +1 for first invocation (before retry)

  }

  class SomeTestClassWithSomeMethod {
    SomeTestClassWithSomeMethod() {
    }

    public Long someMethod(String paramValue1, Integer paramValue2) {
      return (long) 1;
    }


  }

  private int getCurrentRetryAttempt(StatefulRetryHandler retryHandler) throws NoSuchFieldException, IllegalAccessException {
    Field field = StatefulRetryHandler.class.getDeclaredField("currentRetryAttempt");
    field.setAccessible(true);
    return (int) field.get(retryHandler);

  }
}
