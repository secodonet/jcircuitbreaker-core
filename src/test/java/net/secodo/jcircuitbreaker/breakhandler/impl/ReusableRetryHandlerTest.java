package net.secodo.jcircuitbreaker.breakhandler.impl;

import static org.hamcrest.CoreMatchers.equalTo;

import static org.junit.Assert.assertThat;

import static org.mockito.Mockito.mock;

import java.lang.reflect.Field;

import java.util.concurrent.ConcurrentHashMap;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breaker.execution.impl.DefaultExecutionContextImpl;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;

import net.secodo.jcircuitbreaker.task.Task;
import org.junit.Test;


@SuppressWarnings("unchecked")
public class ReusableRetryHandlerTest {
  @Test
  public void shouldCreateNewRetryHandlerForEachInvocation() throws Exception {
    final ReusableRetryHandler<Integer> factory = new ReusableRetryHandler<>(5);

    ExecutionContext<Integer> executionContext1 = new DefaultExecutionContextImpl<>(new ConcurrentHashMap<>(),
      null);

    final BreakHandler<Integer> handler1 = factory.makeHandler(mock(Task.class), executionContext1);
    handler1.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext1);

    ExecutionContext<Integer> executionContext2 = new DefaultExecutionContextImpl<>(new ConcurrentHashMap<>(),
      null);

    final BreakHandler<Integer> handler2 = factory.makeHandler(mock(Task.class), executionContext2);
    handler2.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext2);

    handler2.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext2);

    handler2.onBreak(mock(ContextAwareCircuitBreaker.class),
      mock(Task.class),
      mock(BreakStrategy.class),
      executionContext2);

    assertThat(getNumberOfRetryAttempts((StatefulRetryHandler) handler1), equalTo(1)); // only one call to this handler
    assertThat(getNumberOfRetryAttempts((StatefulRetryHandler) handler2), equalTo(3)); // three calls to this handler

    // NOTE: different state of the handler proves that these are different instances


  }

  private int getNumberOfRetryAttempts(StatefulRetryHandler retryHandler) throws NoSuchFieldException,
                                                                                 IllegalAccessException {
    final Field field = StatefulRetryHandler.class.getDeclaredField("currentRetryAttempt");
    field.setAccessible(true);
    return (int) field.get(retryHandler);
  }

}
