package net.secodo.jcircuitbreaker.task;

/**
 * Represents the java method that should be executed (called <i>target-method</i> in this context). The method can
 * throw any kind of exception.
 *
 * @param <R> the return type of the <i>target-method</i>
 */
public interface Task<R> {

  R execute() throws Exception;
}
