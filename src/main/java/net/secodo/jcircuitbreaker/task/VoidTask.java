package net.secodo.jcircuitbreaker.task;

/**
 * Represents the java method that should be executed (called <i>target-method</i> in this context) which does not
 * return value. The method can throw any kind of exception.
 */
public interface VoidTask extends Task<Void> {

  @Override
  default Void execute() throws Exception {
    executeVoid();
    return null;
  }

  void executeVoid() throws Exception;
  
}
