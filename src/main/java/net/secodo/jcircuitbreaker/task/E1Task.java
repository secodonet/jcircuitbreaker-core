//package net.secodo.jcircuitbreaker.task;
//
///**
// * A variation of {@link Task} which throws only one type of Exception, in contrast to {@link Task} which throws all 
// * possible Exceptions.
// * 
// */
//public interface E1Task<R, E1 extends Exception> extends Task<R> {
//
//  @Override
//  R execute() throws E1;
//}
