package net.secodo.jcircuitbreaker.breakstrategy;

import net.secodo.jcircuitbreaker.breakstrategy.impl.dsl.DslAndStrategy;
import net.secodo.jcircuitbreaker.breakstrategy.impl.dsl.DslOrStrategy;


/**
 * Contains methods (which may be accessed statically) for chaining multiple breaker strategies into single
 * break strategy using "and","or" expressions.
 */
public class SimpleStrategyDsl {
  protected SimpleStrategyDsl() {
  }

  /**
   * Returns {@link BreakStrategy} which returns true if at least one of the given strategies returns true.
   *
   * @param breakStrategies strategies to be chained with "or" statement
   * @param <R> the return type of target-method for which the strategy will be executed
   *
   * @return new strategy built out of given strategies
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public static <R> BreakStrategy<R> anyOf(BreakStrategy... breakStrategies) {
    BreakStrategy<R> resultingStrategy = breakStrategies[0];

    for (int i = 1; i < breakStrategies.length; i++) {
      resultingStrategy = new DslOrStrategy<R>(resultingStrategy, breakStrategies[i]);
    }

    return resultingStrategy;
  }

  /**
   * Returns {@link BreakStrategy} which returns true if all of the given strategies returns true.
   *
   * @param breakStrategies strategies to be chained with "and" statement
   * @param <R> the return type of target-method for which the strategy will be executed
   * @return new strategy built out of given strategies
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public static <R> BreakStrategy<R> allOf(BreakStrategy... breakStrategies) {
    BreakStrategy<R> resultingStrategy = breakStrategies[0];

    for (int i = 1; i < breakStrategies.length; i++) {
      resultingStrategy = new DslAndStrategy<R>(resultingStrategy, breakStrategies[i]);
    }

    return resultingStrategy;
  }
}
