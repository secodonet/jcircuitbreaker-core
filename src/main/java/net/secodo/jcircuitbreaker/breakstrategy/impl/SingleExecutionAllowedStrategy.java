package net.secodo.jcircuitbreaker.breakstrategy.impl;

/**
 * This implementation of {@link net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy} allows execution only if
 * there is no other execution at the moment.
 *
 * @param <R> return value of the target method
 */
public class SingleExecutionAllowedStrategy<R> extends LimitedConcurrentExecutionsStrategy<R> {
  public SingleExecutionAllowedStrategy() {
    super(1);
  }
}
