package net.secodo.jcircuitbreaker.breakstrategy.impl.dsl;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.task.Task;


public class DslOrStrategy<R> implements BreakStrategy<R> {
  private final BreakStrategy<R> strategyA;
  private final BreakStrategy<R> strategyB;

  public DslOrStrategy(BreakStrategy<R> strategyA, BreakStrategy<R> strategyB) {
    this.strategyA = strategyA;
    this.strategyB = strategyB;
  }


  @Override
  public boolean shouldBreak(Task<R> task, ExecutionContext<R> executionContext) {
    return strategyA.shouldBreak(task, executionContext) || strategyB.shouldBreak(task, executionContext);
  }
}
