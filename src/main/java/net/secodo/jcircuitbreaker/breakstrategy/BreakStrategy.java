package net.secodo.jcircuitbreaker.breakstrategy;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.task.Task;


/**
 * Break strategy defines if the circuit breaker should "break". When the "break" occurs <i>real-method</i> is not
 * executed and such situation is handled by break handler.
 *
 * @param <R> the return type of the real method executed by the breaker
 */
public interface BreakStrategy<R> {
  /**
   * Returns true if "break" should happen, so that the Task (target-method) does not execute and circuit
   * breaker executes fallback break handler.
   *
   * @param task the task for execution of which this <i>break strategy</i> was called
   * @param executionContext current execution context
   *
   * @return true if the "break" should happen
   */
  boolean shouldBreak(Task<R> task, ExecutionContext<R> executionContext);
}
