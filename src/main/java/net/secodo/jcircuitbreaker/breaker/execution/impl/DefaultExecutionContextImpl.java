package net.secodo.jcircuitbreaker.breaker.execution.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutedTask;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;


public class DefaultExecutionContextImpl<R> implements ExecutionContext<R> {
  private final ConcurrentHashMap<String, ExecutedTask<R>> tasksInProgress;
  private final Object userData;
  private Map<String, Object> contextAttributes;

  public DefaultExecutionContextImpl(ConcurrentHashMap<String, ExecutedTask<R>> tasksInProgress, Object userData) {
    this.tasksInProgress = tasksInProgress;
    this.userData = userData;
  }


  @Override
  public Collection<ExecutedTask<R>> getExecutionsInProgress() {
    return Collections.unmodifiableCollection(tasksInProgress.values());
  }

  @Override
  @SuppressWarnings("unchecked")
  public <U> U getUserData() {
    return (U) userData;
  }

  @Override
  public <T> void setContextAttribute(String name, T value) {
    ensureAttributesInitialized();
    contextAttributes.put(name, value);
  }

  @Override
  public boolean hasContextAttribute(String name) {
    return (contextAttributes != null) ? contextAttributes.containsKey(name) : false;
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T> T getContextAttribute(String name) {
    return (contextAttributes != null) ? (T) contextAttributes.get(name) : null;
  }

  private void ensureAttributesInitialized() {
    if (contextAttributes == null) {
      contextAttributes = new HashMap<String, Object>();
    }
  }
}
