package net.secodo.jcircuitbreaker.breaker.execution;

import java.util.Objects;
import net.secodo.jcircuitbreaker.task.Task;




/**
 * Class that wraps the real executed method (Task). Represents Task that is or will be under execution by circuit
 * breaker.
 *
 * @param <R> represents the return type of the executed task
 */
public class ExecutedTask<R> {
  private final Task<R> task;
  private final long executionStartedTimestamp;

  public ExecutedTask(Task<R> task, long executionStartedTimestamp) {
    this.task = task;
    this.executionStartedTimestamp = executionStartedTimestamp;
  }

  public long getExecutionStartedTimestamp() {
    return executionStartedTimestamp;
  }

  public Task<R> getTask() {
    return task;
  }


  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof ExecutedTask)) {
      return false;
    }

    ExecutedTask<?> that = (ExecutedTask<?>) other;
    return (executionStartedTimestamp == that.executionStartedTimestamp) &&
      Objects.equals(task, that.task);
  }

  @Override
  public int hashCode() {
    return Objects.hash(task, executionStartedTimestamp);
  }

}
