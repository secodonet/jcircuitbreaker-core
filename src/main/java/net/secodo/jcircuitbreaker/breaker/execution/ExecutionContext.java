package net.secodo.jcircuitbreaker.breaker.execution;

import java.util.Collection;


/**
 * Defines context which is specific to single execution of Task (target method) by circuit breaker. Can be used to
 * share state, get information about currently running tasks by
 * {@link net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy} or
 * {@link net.secodo.jcircuitbreaker.breakhandler.BreakHandler}.
 *
 * @param <R> the return type of real-method that is being executed
 */
public interface ExecutionContext<R> {
  /**
   * Returns collection of Tasks which are currently executed by circuit breaker.
   * @return collection of {@link ExecutedTask}.
   */
  Collection<ExecutedTask<R>> getExecutionsInProgress();

  /**
   * Returns custom data that could haven been passed to the circuit breaker when executing Task. This data is specific
   * only to current execution.
   *
   * @param <U> user custom data passed to the breaker
   * @return custom user data
   */
  <U> U getUserData();

  /**
   * Creates or sets new value for context attribute with given name.
   *
   * @param name the name under which the attribute will be available
   * @param value the value of the atribute
   * @param <T> the type of attribute to set
   */
  <T> void setContextAttribute(String name, T value);

  /**
   * Return true if context attribute with given name exists.
   *
   * @param name name of the attribute that might have been set for current execution
   *
   * @return true if attribute with given name exist
   */
  boolean hasContextAttribute(String name);

  /**
   * Returns value of given context attribute or null of there is no such attribute.
   *
   * @see #hasContextAttribute(String)
   *
   * @param name name of the attribute that might have been set for current execution
   * @param <T> the type to which the value of the attribute with given name should be cast to
   *
   * @return attribute with given name of null if such attribute does not exist
   */
  <T> T getContextAttribute(String name);


}
