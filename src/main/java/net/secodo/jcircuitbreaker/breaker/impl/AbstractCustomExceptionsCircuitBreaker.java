//package net.secodo.jcircuitbreaker.breaker.impl;
//
//import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
//import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
//import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;
//import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
//import net.secodo.jcircuitbreaker.exception.CircuitBreakerException;
//import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
//import net.secodo.jcircuitbreaker.task.Task;
//
//public abstract class AbstractCustomExceptionsCircuitBreaker<R, E1 extends Exception> 
// extends AbstractCircuitBreaker<R>  {
//
//  public <U> R executeWithExceptions(Task<R> task, BreakStrategy<R> breakStrategy, BreakHandler<R> breakHandler,  
//                                 U userData, Class<E1> exceptionClass1) 
//      throws TaskExecutionException, BreakHandlerException, CircuitBreakerException, E1 {
//
//    try {
//      return super.execute(task, breakStrategy, breakHandler, userData);
//    } catch (TaskExecutionException e) {
//      if (exceptionClass1.isInstance(e.getTaskException())) {
//        throw (E1) e.getTaskException();
//      } else {
//        throw e;
//      }
//    } 
//  }
//  
//}
