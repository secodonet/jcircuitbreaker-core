package net.secodo.jcircuitbreaker.breaker.impl;

import net.secodo.jcircuitbreaker.breakhandler.impl.StatefulRetryHandler;

/**
 * Default implementation of the {@link net.secodo.jcircuitbreaker.breaker.CircuitBreaker} which requires
 * each component (ie. {@link net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy},
 * {@link net.secodo.jcircuitbreaker.breakhandler.BreakHandler})
 * to be passed to each invocation of the execute method.
 */
public class DefaultCircuitBreaker<R> extends AbstractCircuitBreaker<R> {
  public DefaultCircuitBreaker() {
    super();
  }

}
