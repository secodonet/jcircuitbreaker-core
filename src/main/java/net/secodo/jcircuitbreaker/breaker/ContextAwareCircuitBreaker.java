package net.secodo.jcircuitbreaker.breaker;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.exception.CircuitBreakerException;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;


/**
 * This interface defines the method which allows to execute a task within given {@link ExecutionContext}.
 *
 * <p>This means that implementing classes are required to pass given {@link ExecutionContext} to both
 * <i>break strategy</i> and <i>break handler</i> since they should run with the given context and not
 * with the new (or other) context.
 *
 * @param <R> the return value that the breaker returns (if breaker wants to return any value)
 */
public interface ContextAwareCircuitBreaker<R> {
  /**
   * Executes given task within given execution context. Implementers of this method are required to use given
   * {@link ExecutionContext} for the whole run of the method. This especially mean that this executionContext needs to
   * be passed to given <i>break strategy</i> and <i>break handler</i>, so that both the strategy and handler are
   * working within given context.
   *
   * @param task encapsulate java <i>target-method</i> which should be executed by this circuit breaker
   * @param breakStrategy the strategy which should define whether task should be executed or break handler should
   *                      handle fallback situation
   * @param breakHandler handles fallback situation
   * @param executionContext already existing context which should be used for execution and be passed to both
   *                         <i>break strategy</i> and <i>break handler</i>
   * @return value returned by executing the Task or fallback value returned by <i>break handler</i>
   * @throws TaskExecutionException in case the task was executed but resulted in exception
   * @throws BreakHandlerException in case <i>break handler</i> was executed but was not able to provide fallback value
   * @throws CircuitBreakerException if unexpected problem occurred while processing the task within CircuitBreaker
   */
  R executeInContext(Task<R> task, BreakStrategy<R> breakStrategy, BreakHandler<R> breakHandler,
                     ExecutionContext<R> executionContext) throws TaskExecutionException, BreakHandlerException,
                                                                  CircuitBreakerException;

}
