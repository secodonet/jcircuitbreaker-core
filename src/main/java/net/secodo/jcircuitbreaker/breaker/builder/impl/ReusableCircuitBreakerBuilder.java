package net.secodo.jcircuitbreaker.breaker.builder.impl;

import net.secodo.jcircuitbreaker.breaker.builder.BuilderValidationException;
import net.secodo.jcircuitbreaker.breaker.impl.ReusableCircuitBreaker;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;


/**
 * A builder class for {@link ReusableCircuitBreaker}. Allows to create instance of {@link ReusableCircuitBreaker}
 * using fluent interface.
 *
 * @param <R> see {@link net.secodo.jcircuitbreaker.breaker.CircuitBreaker}
 */
public class ReusableCircuitBreakerBuilder<R> {
  private BreakStrategy<R> breakStrategy;
  private BreakHandler<R> breakHandler;

  public ReusableCircuitBreakerBuilder() {
  }

  public ReusableCircuitBreakerBuilder<R> withBreakStrategy(BreakStrategy<R> breakStrategy) {
    this.breakStrategy = breakStrategy;
    return this;
  }

  public ReusableCircuitBreakerBuilder<R> withBreakHandler(BreakHandler<R> breakHandler) {
    this.breakHandler = breakHandler;
    return this;
  }

  /**
   * Creates new instance of the breaker.
   *
   * @return newly build circuit breaker
   * @throws BuilderValidationException in case important properties were not set for the circuit breaker that is
   *                                    being created
   */
  public ReusableCircuitBreaker<R> build() throws BuilderValidationException {
    validate();

    return new ReusableCircuitBreaker<R>(breakStrategy, breakHandler);
  }

  private void validate() throws BuilderValidationException {
    if (breakStrategy == null) {
      throw new BuilderValidationException("Break strategy must be set but was null");
    }

    if (breakHandler == null) {
      throw new BuilderValidationException("Break handler must be set but was null");
    }

  }

}
