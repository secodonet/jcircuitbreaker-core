package net.secodo.jcircuitbreaker.breaker.builder;

import net.secodo.jcircuitbreaker.exception.CircuitBreakerException;


public class BuilderValidationException extends CircuitBreakerException {
  private static final long serialVersionUID = -7236579789947333078L;

  public BuilderValidationException(String message) {
    super(message);
  }
}
