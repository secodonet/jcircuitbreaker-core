package net.secodo.jcircuitbreaker.breakhandler.exception;

/**
 * Thrown by implementations of {@link net.secodo.jcircuitbreaker.breakhandler.BreakHandler} to indicate that the
 * break handler failed to handle <i>break</i> and was not able to provide the fallback value.
 */
public class BreakHandlerException extends RuntimeException {
  private static final long serialVersionUID = -8270505759708402748L;

  public BreakHandlerException(String message) {
    super(message);
  }

  public BreakHandlerException(String message, Throwable cause) {
    super(message, cause);
  }
}
