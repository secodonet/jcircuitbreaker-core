package net.secodo.jcircuitbreaker.breakhandler;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.exception.CircuitBreakerException;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;


/**
 * Handles break situation. Can result in different actions to be performed like trying to retry the task or return
 * static fallback value instead.
 *
 * <p>It's main goal is to provide a fallback value for a method in case the CircuitBreaker decided not to execute the
 * real method. If providing a fallback
 * value is not possible break handler should throw {@link BreakHandlerException}
 *
 * @param <R> the return type of onBreak method. This must be the same return type as the of of executed Task
 */
public interface BreakHandler<R> {
  /**
   * Handles situation when break occurs. Returns the fallback value in case the break happens.
   *
   * @param circuitBreaker   a reference to the {@link ContextAwareCircuitBreaker} which called this break handler
   * @param task             the task which execution was prevented by break strategy and resulted in calling this
   *                         break handler
   * @param breakStrategy    the strategy which prevented the execution of task and resulted in calling this break
   *                         handler
   * @param executionContext contains current execution data (specific to current execution)
   * @return fallback value which replaces the value returned by method call
   * @throws TaskExecutionException  in case there was exception while executing the task by this break handler (but
   *                                 only in case this handler decided to execute the task - there is no possibility
   *                                 to throw TaskExecutionException in case there was no attempt to execute a task)
   * @throws CircuitBreakerException in case the break handler tried to execute some task on circuitBreaker but the
   *                                 circuitBreaker fallen with unexpected exception
   *
   * @throws BreakHandlerException   in case this break handler is not able to provide the the fallback value
   */
  R onBreak(ContextAwareCircuitBreaker<R> circuitBreaker, Task<R> task, BreakStrategy<R> breakStrategy,
            ExecutionContext<R> executionContext) throws TaskExecutionException, CircuitBreakerException,
                                                         BreakHandlerException;

}
