package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;


public interface RetryHandlerOnRetryCallback<R> {
  void onRetry(int currentRetryAttempt, ExecutionContext<R> executionContext);

}
