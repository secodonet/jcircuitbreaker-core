package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandlerFactory;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.task.Task;


/**
 * An implementation of {@link BreakHandler} that always returns the value given as a parameter.
 *
 * <p>This implementation can be shared between different executions of circuit breaker and therefore does not require
 * factory method implementing {@link BreakHandlerFactory} to be reusable.
 *
 * @param <R> the return type of actual method
 */
public class ReturnStaticValueHandler<R> implements BreakHandler<R> {
  private final R returnValue;

  public ReturnStaticValueHandler(R returnValue) {
    this.returnValue = returnValue;
  }

  @Override
  public R onBreak(ContextAwareCircuitBreaker<R> circuitBreaker, Task<R> task, BreakStrategy<R> breakStrategy,
                   ExecutionContext<R> executionContext) {
    return returnValue;
  }
}
