package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;


public class RetryHandlerException extends BreakHandlerException {
  private static final long serialVersionUID = 2012506463918787637L;

  public RetryHandlerException(String message) {
    super(message);
  }
}
