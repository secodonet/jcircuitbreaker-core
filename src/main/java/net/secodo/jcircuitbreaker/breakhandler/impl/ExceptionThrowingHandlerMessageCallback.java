package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.task.Task;


public interface ExceptionThrowingHandlerMessageCallback<R> {
  String buildMessage(Task<R> task, ExecutionContext<R> executionContext);

}
