package net.secodo.jcircuitbreaker.breakhandler.impl;

import net.secodo.jcircuitbreaker.breaker.ContextAwareCircuitBreaker;
import net.secodo.jcircuitbreaker.breaker.execution.ExecutionContext;
import net.secodo.jcircuitbreaker.breakhandler.BreakHandler;
import net.secodo.jcircuitbreaker.breakhandler.exception.BreakHandlerException;
import net.secodo.jcircuitbreaker.breakstrategy.BreakStrategy;
import net.secodo.jcircuitbreaker.exception.CircuitBreakerException;
import net.secodo.jcircuitbreaker.exception.TaskExecutionException;
import net.secodo.jcircuitbreaker.task.Task;

/**
 * An implementation of {@link BreakHandler} that does completely nothing. Can be used when it is not really
 * important whether the Task was actually executed by circuit breaker or not. Useful for example for "fire and
 * forget methods" - see example below. It's 
 * {@link #onBreak(ContextAwareCircuitBreaker, Task, BreakStrategy, ExecutionContext)} method returns null. 
 *
 * <p>Example: Suppose, there is a method that sends a "ping" message (or HTTP HEAD request) to distant server, just to
 * notify the server about some event. However for the application it is not really important if the ping was
 * received by distant server or not. Since the ping can take some time (for example the network can occasionally be
 * slow) such ping method can be protected by circuit breaker. For this use case <i>break strategy</i> is created which
 * checks if at least one out of 3 recently sent pings succeeded within last 5 minutes. If not, "break" happens and
 * <i>break handler</i> is executed. Since we are not interested in doing anything when "break" happens
 * {@link NoActionHandler} is used.
 *
 * <p>In above example please note that <i>break strategy</i> will check intervals of 5 minutes for 3 successful
 * calls, so after 5 minutes the ping can be sent again, because the break strategy will "reset".
 */
public class NoActionHandler<R> implements BreakHandler<R> {
  @Override
  public R onBreak(ContextAwareCircuitBreaker<R> circuitBreaker, Task<R> task, BreakStrategy<R> breakStrategy,
                   ExecutionContext<R> executionContext)
    throws TaskExecutionException, CircuitBreakerException, BreakHandlerException {
    return null;
  }
}
