package net.secodo.jcircuitbreaker.util;

public class TimeUtil {
  /**
   * Convenience method. Can be mocked in tests.
   *
   * @return current system time.
   */
  public long getCurrentTimeMilis() {
    return System.currentTimeMillis();
  }
}
