package net.secodo.jcircuitbreaker.exception;

/**
 * Thrown by {@link net.secodo.jcircuitbreaker.breaker.CircuitBreaker} when the call to <i>real-method</i> resulted in
 * exception. The original exception (except for being set as a cause of this exception) is available via
 * {@link #getTaskException()}
 */
public class TaskExecutionException extends Exception {
  private static final long serialVersionUID = 3223125370437887256L;
  private final Throwable taskException;

  public TaskExecutionException(String message, Throwable taskException) {
    super(message, taskException);
    this.taskException = taskException;
  }

  public Throwable getTaskException() {
    return taskException;
  }
}
