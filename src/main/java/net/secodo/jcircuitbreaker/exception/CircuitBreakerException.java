package net.secodo.jcircuitbreaker.exception;

public class CircuitBreakerException extends RuntimeException {
  private static final long serialVersionUID = 108008587096013224L;

  public CircuitBreakerException(String message) {
    super(message);
  }

  public CircuitBreakerException(String message, Throwable cause) {
    super(message, cause);
  }

}
