## About

The idea behind JCircuitBreaker was born after some potential load issues were identified on a running system.  Most 
 of the original code was developed by Andrzej Martynowicz. A lot of valuable insight on potential usage scenarios 
 was brought to the project by Ravi Yadav, Kamil Woś and Bartosz Małecki. Many thanks for the support for the project 
 goes to 
 [Immobilienscout24](https://www.immobilienscout24.de/). Without 
their significant input the development would have been much slower and possible releases would have been postponed.
 
Originally designed and developed by Andrzej Martynowicz with help from developers from 
[Brighone OSS](https://www.brightone.pl). 
Currently hosted and developed by community at [secodo.net](http://secodo.net). 
 
If you want to submit a bug or feature request please use project
[issue tracker](https://bitbucket.org/secodonet/jcircuitbreaker-core/issues) hosted by Atlassian Bitbucket.

## Contact information

To get in touch, please contact either with Andrzej Martynowicz via 
[LinkedIn profile](https://pl.linkedin.com/in/andrzejmartynowicz) or use contact form at 
[secodo.net](http://secodo.net).
 
