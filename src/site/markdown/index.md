## Introduction to JCircuitBreaker

JCircuitBreaker is a java library loosely implementing Circuit Breaker pattern. Allows to reduce load on specified 
resource - defined as a call to java method - in case there is a risk that the method might not finnish in reasonable
 time manner or in case some special (custom) conditions takes place. These "conditions", which are called 
 _break strategy_, are fully customizable and specified individually for each circuit breaker. 
 Some predefined implementations of _break strategy_ are already shipped together 
 with the library. Many more are targeted for future releases. The java method that is
 protected by the circuit breaker is called a _target-method_ or _Task_ 
 (written with capital letter). The role of the circuit breaker is to decide at runtime weather the _target-method_ 
 should be executed or not (depending on what _break strategy_ decides at the moment).

Such mechanism is particularly useful when _target-method_ is potentially long running task or a task which can
 consume a lot of server resources (like an expensive database call) and therefore only limited number of such tasks 
 should be executed in parallel to avoid overhead. Also a call to an external rest-api s a 
 good candidate to be protected by a circuit breaker since the response time from external resource may be long.

More information can be found on [usage page](usage.html).

The JCircuitBreaker library is Open Source library developed by [secodo.net](http://secodo.net) team and is 
distributed under [Apache Software License 2.0](https://www.apache.org/licenses/LICENSE-2.0).

### Technical notes

#### Versioning

Library version consists of three numbers A.B.C. A is library version number which can change when big changes are 
applied to the library. B signifies major version. Major version changes when new features are introduced to the 
library. When  
major version changes there might be small backwards-compatibility problems for which migration guide is provided. C is 
minor
version number. Minor version changes when there are minor updates to the library like bug fixes or small new 
features which are backward compatible. Minor version is guaranteed to be backward compatible. 
     
#### Changelog

##### version 1.2.0

* new versioning introduced
* Major refactoring of the circuit breaker mechanism 
* TaskExecutionException is thrown by execute method of CircuitBreaker#execute method and signifies the problem only with actual task execution (i.e. thrown when execution of real method resulted in exception)
* CircuitBreaker exception thrown by CircuitBreaker#execute method is now RuntimeException
* FixedCircuitBreaker is now renamed to ReusableCircuitBreaker
* RetryHandler has onRetry() method which can be overriden in subclasses
* static JCircuitBreaker was removed because, when called from different locations, shared executions of different types of tasks which resulted in not expected behaviours of 
break strategies which depended on the currently executed tasks
* BreakHandlerFactory interface is provided to allow creation of BreakHandlers at runtime 
* OnePerExecutionHandlerFactory interface is provided to allow creation of one instance of given BreakHandler per 
single execution of circuit breaker 
* ReusableCircuitBreakerBuilder is provided to allow creation of ReusableCircuitBreaker in "fluent interface way"
* Task interface was introduced and replaces the Callable interface
* VoidTask interface was introduced to support void methods
* TooManyConcurrentExecutionsStrategy renamed to LimitedConcurrentExecutionsStrategy
* TooLongCurrentAverageExecutionTimeStrategy renamed to LimitedCurrentAverageExecutionTimeStrategy


##### migration from version 1.1

* change name of FixedCircuitBreaker to ReusableCircuitBreaker and make sure you read the api of 
ReusableCircuitBreaker
* catch TaskExecutionException instead of CircuitBreakerException when executing your methods (read more below)
* replace static calls to JCircuitBreaker.execute() by creating new instances of DefaultCircuitBreaker or 
ReusableCircuitBreaker
* methods to execute should now be casted to Task class and not Callable class
* Rename TooManyConcurrentExecutionsStrategy to LimitedConcurrentExecutionsStrategy
* Rename TooLongCurrentAverageExecutionTimeStrategy to LimitedCurrentAverageExecutionTimeStrategy



##### version 1.1
* Minor changes - moved project to secodo.net, artifact groupId changed to net.secodo.jcircuitbreaker

##### version 1.0
* Stable version - released


##### version 1.0-RC3
* FixedCircuitBreaker class added
* Changed visibility for fields inside strategy classes
* Fixed RetryHandler bug
* Added two new break handlers (ReturnStaticValueHandler and ExceptionThrowingHandler)



