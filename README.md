#### Circuit breaker library

JCircuitBreaker is a java library loosely implementing Circuit Breaker pattern. Allows to limit load at specified 
resource - defined as a call to java method - in case there is a risk that the method might not finnish in reasonable
 time manner or in case some special (custom) conditions takes place. These "conditions", are called _break 
 strategy_, are fully customizable and specified individually for each circuit breaker which executes the 
 java method. Some predefined implementations of _break strategy_ are already shipped together 
 with the library. Many more are targeted for future releases (contributions are welcome). The java method that is
 protected by the circuit breaker is called_target-method_ (written with dash) or _Task_ 
 (written with capital letter). The role of the circuit breaker is to decide at runtime weather the _target-method_ 
 should be executed or not (thanks to the _break strategy_).
 
 Refer to documentation at
 [http://secodo.net/projects/jcircuitbreaker](http://secodo.net/projects/jcircuitbreaker)
 learn more. Documentation is hosted by [secodo.net](http://secodo.net). 
 
#### Developers notes
 
 To generate full site run 
 
 ```
 mvn package site
 ```
 The package goal is needed to generate JaCoCo site report as JaCoCo requires unit tests to be executed to generate 
 coverage reports (site goal does not run tests directly).
 
 Except for JaCoCo report, generated site contains also Checkstyle report according to secodo_checks.xml. The release
 requires reasonable coverage and NO checkstyle violations.

  To locally install snapshot release package run 
  
  ```
  mvn clean site install
  ```


#### Changelog

##### version 1.2.0

* new versioning introduced with last digit for bug fixes and minor changes
* Major refactoring of the circuit breaker mechanism 
* TaskExecutionException is thrown by execute method of CircuitBreaker#execute method and signifies the problem only with actual task execution (i.e. thrown when execution of real method resulted in exception)
* CircuitBreaker exception thrown by CircuitBreaker#execute method is now RuntimeException
* FixedCircuitBreaker is now renamed to ReusableCircuitBreaker
* RetryHandler has onRetry() method which can be overriden in subclasses
* static JCircuitBreaker was removed because, when called from different locations, shared executions of different types of tasks which resulted in not expected behaviours of 
break strategies which depended on the currently executed tasks
* BreakHandlerFactory interface is provided with to allow creation of BreakHandlers at runtime 
* OnePerExecutionHandlerFactory interface is provided to allow creation of one instance of given BreakHandler per 
single execution of circuit breaker 
* ReusableCircuitBreakerBuilder is provided to allow creation of ReusableCircuitBreaker in "fluent interface way"
* Task interface was introduced and replaces the Callable interface
* VoidTask interface was introduced to support void methods
* TooManyConcurrentExecutionsStrategy renamed to LimitedConcurrentExecutionsStrategy
* TooLongCurrentAverageExecutionTimeStrategy renamed to LimitedCurrentAverageExecutionTimeStrategy

##### migration from version 1.1

* change name of FixedCircuitBreaker to ReusableCircuitBreaker and make sure you read the api of 
ReusableCircuitBreaker
* catch TaskExecutionException instead of CircuitBreakerException when executing your methods (read more below)
* replace static calls to JCircuitBreaker.execute() by creating new instances of DefaultCircuitBreaker or 
ReusableCircuitBreaker
* methods to execute should now be casted to Task class and not Callable class
* Rename TooManyConcurrentExecutionsStrategy to LimitedConcurrentExecutionsStrategy
* Rename TooLongCurrentAverageExecutionTimeStrategy to LimitedCurrentAverageExecutionTimeStrategy


##### version 1.1
* Minor changes - moved project to secodo.net, artifact groupId changed to net.secodo.jcircuitbreaker

##### version 1.0
* Stable version - released


##### version 1.0-RC3
* FixedCircuitBreaker class added
* Changed visibility for fields inside strategy classes
* Fixed RetryHandler bug
* Added two new break handlers (ReturnStaticValueHandler and ExceptionThrowingHandler)
